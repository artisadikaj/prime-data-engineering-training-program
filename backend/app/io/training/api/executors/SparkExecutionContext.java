package io.training.api.executors;

import akka.actor.ActorSystem;
import com.google.inject.Inject;
import play.libs.concurrent.CustomExecutionContext;

/**
 * Created by agonlohaj on 30 Oct, 2019
 */
public class SparkExecutionContext extends CustomExecutionContext {

	@Inject
	public SparkExecutionContext(ActorSystem actorSystem) {
		// uses a custom thread pool defined in application.conf
		super(actorSystem, "spark-executor");
	}
}