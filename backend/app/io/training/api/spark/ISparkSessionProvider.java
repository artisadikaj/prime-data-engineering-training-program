package io.training.api.spark;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Map;

/**
 * Created by agonlohaj on 22 Aug, 2019
 */
public interface ISparkSessionProvider {
	/**
	 * Return a spark session to use for computations
	 * @return session - SparkSession
	 */
	public abstract SparkSession getSparkSession();

	public Map<String, Dataset<Row>> getDatasets();

	/**
	 * Close an active spark session
	 */
	public void closeActiveSessions();
}
