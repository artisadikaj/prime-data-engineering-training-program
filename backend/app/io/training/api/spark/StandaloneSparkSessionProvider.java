package io.training.api.spark;

import akka.Done;
import akka.actor.CoordinatedShutdown;
import com.google.inject.Inject;
import com.typesafe.config.Config;
import lombok.Data;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import scala.collection.JavaConverters;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by agonlohaj on 22 Aug, 2019
 */
@Data
public class StandaloneSparkSessionProvider implements ISparkSessionProvider {
    protected Config config;
    private SparkSession sparkSession;
    private Map<String, Dataset<Row>> datasets;

    @Inject
    public StandaloneSparkSessionProvider(CoordinatedShutdown coordinatedShutdown, Config config) {
        coordinatedShutdown.addTask(CoordinatedShutdown.PhaseServiceStop(), "shutting-down-spark", () -> {
            this.closeActiveSessions();
            return CompletableFuture.completedFuture(Done.done());
        });
        this.config = config;
//		Logger.getLogger("org").setLevel(Level.ERROR);
    }

    @Override
    public SparkSession getSparkSession() {
        if (sparkSession != null) {
            return sparkSession;
        }
//		System.setProperty("hadoop.home.dir", arguments.getHadoopHome());
        System.setProperty("com.amazonaws.services.s3.enableV4", "true");

        SparkSession.Builder builder = SparkSession.builder()
                .appName("Prime Data Training Program")
                .config("spark.master", config.getString("spark.master"));

        sparkSession = builder.getOrCreate();

        scala.collection.immutable.Map<String, String> sparkConfig = sparkSession.conf().getAll();

        Map<String, String> configuration = JavaConverters.mapAsJavaMap(sparkConfig);
        for (String key : configuration.keySet()) {
            System.out.println("Key : " + key + " value: " + configuration.get(key));
        }

        populateDatasets();

        return sparkSession;
    }

    private void populateDatasets() {
        if (datasets != null) {
            return;
        }

        Function<String, Dataset<Row>> mapping = (String csvName) -> this.getSparkSession()
                .read()
                .option("sep", ",").option("header", true)
                .csv(String.format("%s%s.csv", "data/s3.spark.assignments_df/", csvName));

        this.datasets = Stream.of("customers", "employees", "order_details", "orders", "products")
                .collect(Collectors.toMap(Function.identity(), mapping));

        this.datasets.forEach((name, ds) -> ds.createOrReplaceTempView(name));
    }

    public void closeActiveSessions() {
        if (sparkSession == null) {
            return;
        }
        try {
            sparkSession.close();
            sparkSession.sparkContext().stop();
        } catch (Exception ignored) {
        }
    }
}
