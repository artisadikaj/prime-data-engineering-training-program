package io.training.api.utils;

/**
 * Created by Agon on 10/17/2016.
 */
public class Constants {
    public static final int PAGE_NUMBER = 30;
    public static final int PAGE_NUMBER_LONG = 100;
    public static final String COUNT_KEY = "count";
    // Oct 1, 2015 12:00:22 AM
    // 2014-03-03 00:00:00 +0200
    public static final String DATE_TIME_PARSING_FORMAT = "yyyy-MM-dd HH:mm:ss Z";
    public static final String DATE_TIME_CLIENT_FORMAT = "dd-MM-yyyy HH:mm:ss Z";
    public static final String DATE_CLIENT_FORMAT = "dd-MM-yyyy HH:mm:ss";
    public static final String PRESTO_DATE_FORMAT = "yyyy-MM-dd";
    // batch size on mongo query
    public static final int BATCH_SIZE = 10000;

}
