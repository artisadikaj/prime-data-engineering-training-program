package io.training.api.services;

import com.google.inject.Inject;
import io.training.api.executors.SparkExecutionContext;
import io.training.api.models.requests.BinarySearchRequest;
import io.training.api.models.requests.NameValuePair;
import io.training.api.models.responses.ChartData;
import io.training.api.spark.ISparkSessionProvider;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import play.libs.concurrent.HttpExecutionContext;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.apache.spark.sql.functions.*;

/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class AssignmentThreeService {
    @Inject
    SparkExecutionContext sparkExecutionContext;
    @Inject
    ISparkSessionProvider provider;
    @Inject
    HttpExecutionContext ec;

    /**
     * Function as Line: y = x * 2
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Integer>> function1(List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            JavaSparkContext sc = new JavaSparkContext(session.sparkContext());
            JavaRDD<Integer> result = sc.parallelize(input).map(x -> x * 2);
            return result.collect();
        }, sparkExecutionContext);
    }


    /**
     * Function as Scatter y = square root of the absolute value of ((x ^ 2) + (x * 4))
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function2(List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            return new ArrayList<>();
        }, sparkExecutionContext);
    }

    /**
     * Function y = If 3^2 - x^2 > 0 than square root of (3^2 - x^2). If 3^2 - x^2 < 0 then - square root of absolute value of (3^2 - x^2)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function3(List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            return new ArrayList<>();
        }, sparkExecutionContext);
    }

    /**
     * Function as Line: y = sin(x)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function4(List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            return new ArrayList<>();
        }, sparkExecutionContext);
    }

    /**
     * Function as Line: y = cos(x)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function5(List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            return new ArrayList<>();
        }, sparkExecutionContext);
    }

    /**
     * 2 Line Functions displayed together, one for Sin and one for Cos, check only the series option to include two of them
     * Returns two lists of double the first list for sin, and the second one for cos
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<List<Double>>> function6(List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            List<List<Double>> result = new ArrayList<>();
            return result;
        }, sparkExecutionContext);
    }

    /**
     * I want to see the top 4 performing words, given the randomCategoryData, the top 4 with the highest random generated value
     * Make sure the values are summed on repeated words (no duplicates)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function7(List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            return new ArrayList<>();
        }, sparkExecutionContext);
    }

    /**
     * Calculate the average within the groups now, and show that here. Check the random Category data on how it generates those
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<ChartData>> function8(List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            return new ArrayList<>();
        }, sparkExecutionContext);
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function9(List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            return new ArrayList<>();
        }, sparkExecutionContext);
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     *
     * @param request
     * @return index - the index of the search
     */
    public CompletableFuture<Integer> binarySearch(BinarySearchRequest request) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return -1;
        }, ec.current());
    }

    /**
     * In the following assignments, you will have to use these tables:
     *
     * customers        ->  `CustomerID` STRING,    `CustomerName` STRING,  `ContactName` STRING,   `Address` STRING,
     *                      `City` STRING,          `PostalCode` STRING,    `Country` STRING
     *
     * employees        ->  `EmployeeID` STRING,    `LastName` STRING,      `FirstName` STRING,     `BirthDate` STRING,
     *                      `Photo` STRING,         `Notes` STRING
     *
     * orders           ->  `OrderID` STRING,       `CustomerID` STRING,    `EmployeeID` STRING,    `OrderDate` STRING,
     *                      `ShipperID` STRING
     *
     * order_details    ->  `OrderDetailID` STRING, `OrderID` STRING,       `ProductID` STRING,     `Quantity` STRING
     *
     * products         ->  `ProductID` STRING,     `ProductName` STRING,   `SupplierID` STRING,    `CategoryID` STRING,
     *                      `Unit` STRING,`Price` STRING
     *
     * To use a dataset, you can use provider.getDatasets().get(dataset) where `dataset` can be the names
     * explained above.
     */


    /**
     * Assignments 1: Show full name (`FirstName` and `LastName`) of the eldest employee
     */
    public CompletableFuture<Dataset<Row>> assignment1() {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            Dataset<Row> employee = provider.getDatasets().get("employees");

            return employee
                    .orderBy(col("BirthDate").desc())
                    .limit(1)
                    .select("FirstName", "LastName");
        }, sparkExecutionContext);
    }

    /**
     * Assignment 2: Find the categories (`CategoryID`, `SupplierID`, `Amount`) that have sales of over 50.000,
     * excluding sales supplier `SupplierId` = 1. (`Quantity` * `Price` > 50.000)
     */
    public CompletableFuture<Dataset<Row>> assignment2() {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return null;
        }, sparkExecutionContext);
    }

    /**
     * Assignment 3: Find the top 10 customers (`CustomerName`, `Address` and `City`) who have ordered the most
     */
    public CompletableFuture<List<String>> assignment3() {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return null;
        }, sparkExecutionContext);
    }

    /**
     * Assignment 4: Find the customers (`CustomerName`, `ContactName`, `NrOfDays`) that have ordered the most in consecutive days
     */
    public CompletableFuture<Dataset<Row>> assignment4() {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            return null;
        }, sparkExecutionContext);
    }

    /**
     * Assignment 5: Find all the days that the customer did not do an order from the first date to the
     * last that he purchased
     */
    public CompletableFuture<Dataset<Row>> assignment5() {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            return null;
        }, sparkExecutionContext);
    }

    public CompletableFuture<List<String>> assignment6() {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return Arrays.asList(
                    "1. ",
                    "2. ",
                    "3. ",
                    "4. ",
                    "5. ",
                    "6. ",
                    "7. ",
                    "8. ",
                    "9. ",
                    "10. "
            );
        }, sparkExecutionContext);
    }
}
