package io.training.api.models.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by agonlohaj on 20 Aug, 2020
 */
@Data
@AllArgsConstructor
public class EmptyResponse {
}
