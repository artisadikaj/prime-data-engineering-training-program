package io.training.api.controllers;

import com.google.inject.Inject;
import io.training.api.services.LectureThreeService;
import io.training.api.services.SerializationService;
import io.training.api.utils.DatabaseUtils;
import play.mvc.*;

import java.util.concurrent.CompletableFuture;

public class LectureThreeController extends Controller {
	@Inject
	SerializationService serializationService;

	@Inject
	LectureThreeService service;

	public CompletableFuture<Result> exercise1 (Http.Request request) {
		return service.exercise1()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> exercise2 (Http.Request request) {
		return service.exercise2()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> exercise3 (Http.Request request) {
		return service.exercise3()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> exercise4 (Http.Request request) {
		return service.exercise4()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> exercise5 (Http.Request request) {
		return service.exercise5()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> exercise6 (Http.Request request) {
		return service.exercise6()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}
}