package io.training.api.controllers;

import com.google.inject.Inject;
import io.training.api.services.AssignmentFourService;
import io.training.api.services.SerializationService;
import io.training.api.utils.DatabaseUtils;
import play.mvc.*;

import java.util.concurrent.CompletableFuture;

public class AssignmentLectureFourController extends Controller {

	@Inject
	SerializationService serializationService;

	@Inject
	AssignmentFourService service;

	public CompletableFuture<Result> sqlPlayground (Http.Request request) {
		return service.sqlPlayground()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> assignment1 (Http.Request request) {
		return service.assignment1()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> assignment2 (Http.Request request) {
		return service.assignment2()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> assignment3 (Http.Request request) {
		return service.assignment3()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> assignment4 (Http.Request request) {
		return service.assignment4()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> assignment5 (Http.Request request) {
		return service.assignment5()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

	public CompletableFuture<Result> knightTour (Http.Request request) {
		return service.knightTour()
				.thenCompose((data) -> serializationService.toJsonNode(data))
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}
}