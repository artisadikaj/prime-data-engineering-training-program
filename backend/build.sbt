organization := "io.goprime.training"
name := "data-training"

version := "1.0.0"

routesGenerator := InjectedRoutesGenerator

PlayKeys.devSettings += "config.resource" -> "development.conf"

fork in Test := true
javaOptions in Test ++= Seq("-Dconfig.file=conf/test.conf", "-Dlogger.resource=test.logback.xml")

scalaVersion := "2.12.9"

val akkaManagementVersion = "1.0.0"
val akkaVersion = "2.6.0"
val akkaHTTPVersion = "10.1.10"

sources in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false

libraryDependencies ++= Seq(
  javaWs,
  guice,
  ehcache,
  javaJdbc,
  filters,
  "junit" % "junit" % "4.12",
  "org.mongodb" % "mongodb-driver-sync" % "4.3.0",
  "org.projectlombok" % "lombok" % "1.18.12",
  "de.flapdoodle.embed" % "de.flapdoodle.embed.mongo" % "2.2.0",
  "com.typesafe.play" %% "play-mailer" % "6.0.1",
  "com.typesafe.play" %% "play-mailer-guice" % "6.0.1",
  "org.hibernate" % "hibernate-validator" % "6.1.5.Final",
  "org.glassfish" % "javax.el" % "3.0.0",
  // spark related stuff
  "org.apache.hadoop" % "hadoop-aws" % "3.2.1",
  "org.apache.hadoop" % "hadoop-client" % "3.2.1",
  "org.apache.hadoop" % "hadoop-hdfs" % "3.2.1",
  "org.apache.spark" %% "spark-core" % "3.1.2",
  "org.apache.spark" %% "spark-hive" % "3.1.2",
  "org.apache.spark" %% "spark-sql" % "3.1.2",
  "org.apache.spark" %% "spark-streaming" % "3.1.2",
  "org.mongodb.spark" %% "mongo-spark-connector" % "3.0.1",
  "org.apache.spark" %% "spark-mllib" % "3.0.1" % "provided",
  "com.redislabs" %% "spark-redis" % "3.0.0",

  "com.github.karelcemus" %% "play-redis" % "2.5.0",
  // swagger
  "io.swagger" %% "swagger-play2" % "1.7.1"
)


resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
resolvers += Resolver.url("Typesafe Ivy releases", url("https://repo.typesafe.com/typesafe/ivy-releases"))(Resolver.ivyStylePatterns)
resolvers += Resolver.typesafeRepo("releases")
resolvers += Resolver.sbtPluginRepo("releases")
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

lazy val root = project.in(file(".")).enablePlugins(PlayScala)
