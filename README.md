
This repo contains the following:
1. The front end interactive web which contains resources, content and materials that will be used through the training program.
2. The back end code repository which will contain all the backend code of assignments, examples and more

In order to get started, please refer to the README files of the frontend and backend folders

Gitlab Pages
--------------------

The repository is available at the following location: https://agonlohaj.gitlab.io/prime-data-engineering-training-program/.
Refreshing the page currently doesn't work at a specific location, you always have to start over from the base url due to the static pages configuration of GitLab!

Documentation
--------------------
This project itself is a documentation of the training program


"Your Name Here"
---------------------