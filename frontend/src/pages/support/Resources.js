/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
const styles = ({ typography }) => ({
  root: {},
})

class Resources extends React.Component {
  render() {
    const { classes } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Resources
          <Divider />
        </Typography>
        <Typography variant='p'>
          Setup and Install
          <ul>
            <li><label>Install Source Tree: <SimpleLink href="https://www.sourcetreeapp.com/">https://www.sourcetreeapp.com/</SimpleLink></label></li>
            <li><label>Install Visual Studio Code: <SimpleLink href="https://code.visualstudio.com/">https://code.visualstudio.com/</SimpleLink></label></li>
            <li><label>Install IntelliJ: <SimpleLink href=" https://www.jetbrains.com/idea/">https://www.jetbrains.com/idea/</SimpleLink></label></li>
            <li><label>Slack Install: <SimpleLink href="https://slack.com/download">https://slack.com/download</SimpleLink></label></li>
            <li><label>GitLab: <SimpleLink href="https://gitlab.com">https://gitlab.com</SimpleLink></label></li>
            <li><label>GIT: <SimpleLink href="https://git-scm.com/">https://git-scm.com/</SimpleLink></label></li>
            <li><label>Working with GIT: <SimpleLink href="https://product.hubspot.com/blog/git-and-github-tutorial-for-beginners">https://product.hubspot.com/blog/git-and-github-tutorial-for-beginners</SimpleLink></label></li>
            <li><label>JSON: <SimpleLink href="https://www.json.org/">https://www.json.org/</SimpleLink></label></li>
            <li><label>Scala Build Tool (SBT): <SimpleLink href="https://www.scala-sbt.org/">https://www.scala-sbt.org/</SimpleLink></label></li>
            <li><label>Java Releases: <SimpleLink href="http://www.oracle.com/technetwork/java/javase/downloads/">http://www.oracle.com/technetwork/java/javase/downloads/</SimpleLink></label></li>
            <li><label>Spark Maven Dependencies: <SimpleLink href="https://mvnrepository.com/artifact/org.apache.spark">https://mvnrepository.com/artifact/org.apache.spark</SimpleLink></label></li>
          </ul>
          Books:
          <ul>
            <li><label>Spark: The Definitive Guide</label></li>
            <li><label>Hadoop: The Definitive Guide</label></li>
            <li><label>Amazon Web Services In Action</label></li>
          </ul>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Resources)
