/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import HiveArchitectureImage from 'assets/images/lecture2/hive_architecture.png'
import { Bold } from 'presentations/Label'
import SimpleLink from 'presentations/rows/SimpleLink'

const styles = ({ typography }) => ({
  root: {},
})

class Hive extends React.Component {
  render() {
    const { classes, section } = this.props
    const architecture = section.children[0]
    const hiveQL = section.children[1]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography>
          Hive is a data warehouse infrastructure tool to process structured data in Hadoop. It resides on top of Hadoop to summarize Big Data, and makes querying and analyzing easy. Hive is targeted towards users who are comfortable with SQL. It is similar to SQL and called HiveQL, used for managing and querying structured data. Apache Hive is used to abstract complexity of Hadoop. This language also allows traditional map/reduce programmers to plug in their custom mappers and reducers.
        </Typography>
        <Typography>
          Hive, an open source peta-byte scale date warehousing framework based on Hadoop, was developed by the Data Infrastructure Team at Facebook. Let’s look at some of its features that makes it popular and user friendly:
          <ul>
            <li>Allows programmers to plug in custom Mappers and Reducers.</li>
            <li>Has Data Warehouse infrastructure.</li>
            <li>Provides tools to enable easy data ETL.</li>
            <li>Defines SQL-like query language called QL.</li>
          </ul>
          With hive you are able to perform the following:
          <ul>
            <li>Tables can be portioned and bucketed.</li>
            <li>Schema flexibility and evolution.</li>
            <li>JDBC/ODBC drivers are available.</li>
            <li>Hive tables can be defined directly in the HDFS.</li>
            <li>Extensible – Types, Formats, Functions and scripts.</li>
          </ul>
        </Typography>
        <Typography id={architecture.id} variant={'title'}>
          {architecture.display}
        </Typography>
        <img src={HiveArchitectureImage}/>
        <Typography>
          Hive consists of the following major components:
          <ul>
            <li>
              <Bold>Metastore</Bold>: It is the repository for metadata. This metadata consists of data for each table like its location and schema. It also holds the information for partition metadata which lets you monitor the various distributed data progress in the cluster. This data is generally present in the relational databases. The metadata keeps track of the data, replicates the data and provides a backup in case of data loss.
            </li>
            <li>
              <Bold>Driver</Bold> The driver receives the HiveQL statements and works like a controller. It monitors the progress and life cycle of various executions by creating sessions. When a HiveQL statement is executed the Driver stores the metadata generated while executing the statement. When the reducing operation is completed by the MapReduce job the driver collects the data points and query results.
            </li>
            <li>
              <Bold>Compiler</Bold>: The Compiler is assigned with the task of converting the HiveQL query into MapReduce input. It includes a method to execute the steps and tasks needed to let the HiveQL output as needed by the MapReduce.
            </li>
            <li>
              <Bold>Optimizer</Bold>: This performs the various transformation steps for aggregating, pipeline conversion by a single join for multiple joins. It also is assigned with the task of splitting a task while transforming the data before the reduce operations for improved efficiency and scalability.
            </li>
            <li>
              <Bold>Executor</Bold>: The Executor executes the tasks after the compilation and the optimization steps. The Executor directly interacts with the Hadoop Job Tracker for scheduling of tasks to be run.
            </li>
            <li>
              <Bold>CLI</Bold>, UI, and Thrift Server: the Command Line Interface and the User Interface submits the queries, process monitoring and instructions so that external users can interact with Hive. Thrift lets other clients to interact with Hive.
            </li>
          </ul>
        </Typography>
        <Typography id={hiveQL.id} variant={'title'}>
          {hiveQL.display}
        </Typography>
        <Typography>
          The Hive query language is called the HiveQL but is not exactly a structured query language. But HiveQL offers various other extensions that are not part of the SQL. You can create multi-table inserts, create table as select but it has only a basic support for indexes. But HiveQL does not provide support for Online Transaction Processing and view materialization. It only offers sub-query support. Now it is possible to have full ACID properties along with update, insert and delete functionalities.
        </Typography>
        <Typography>
          The way in which Hive stores and queries data has a close resemblance with the regular databases. But since Hive is built on top of the Hadoop ecosystem it has to adhere to the rules set forth by the Hadoop framework.
        </Typography>
        <Typography>
          Use the following link to learn more about how to use Hive: <SimpleLink href="https://cwiki.apache.org/confluence/display/Hive/LanguageManual">Language Manual</SimpleLink>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Hive)
