/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import React from "react";
import Intro from "pages/lecture2/Intro";
import Introduction from "pages/lecture2/Introduction";
import Hive from "pages/lecture2/Hive";
import Hadoop from "pages/lecture2/Hadoop";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_2.HIVE:
        return <Hive {...props} />
      case PAGES.LECTURE_2.HADOOP:
        return <Hadoop {...props} />
      case PAGES.LECTURE_2.INTRODUCTION:
        return <Introduction {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
