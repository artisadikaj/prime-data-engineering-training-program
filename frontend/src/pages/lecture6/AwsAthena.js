/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import Code from "presentations/Code";
import AwsAthenaImg from 'assets/images/lecture6/aws_athena.png'
import S3GlueAthenaArchitectureImg from 'assets/images/lecture6/s3_glue_athena_architecture.png'

const styles = ({ typography }) => ({
  root: {},
})

const athenaTableCreation = `CREATE EXTERNAL TABLE \`table_name\`(
  \`level\` string, 
  \`datetime\` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY ',' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://<bucket-name>/raw'
TBLPROPERTIES (
  'has_encrypted_data'='false', 
  'skip.header.line.count'='1')
`

class AwsAthena extends React.Component {
  render() {
    const { classes, section } = this.props
    const idealUsagePatterns = section.children[0]
    const performance = section.children[1]
    const scalabilityAndElasticity = section.children[2]
    const referenceArchitecture = section.children[3]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          <img src={AwsAthenaImg} />
        </Typography>

        <Typography>
          Amazon Athena is an interactive query service that makes it easy to analyze data in Amazon S3 using
          standard SQL. Athena is serverless, so there is no infrastructure to setup or manage, and you can start
          analyzing data immediately. You don’t need to load your data into Athena, as it works directly with data
          stored in S3. Just log into the Athena Console, define your table schema, and start querying. Amazon Athena
          uses Presto with full ANSI SQL support and works with a variety of standard data formats, including CSV,
          JSON, ORC, Apache Parquet, and Apache Avro.
        </Typography>

        <Typography id={idealUsagePatterns.id} variant={'title'}>
          {idealUsagePatterns.display}
        </Typography>

        <Typography>
          <ul>
            <li>
              Interactive ad hoc querying for web logs – Athena is a good tool for interactive one-time SQL queries
              against data on Amazon S3. For example, you could use Athena to run a query on web and application
              logs to troubleshoot a performance issue. You simply define a table for your data and start querying
              using standard SQL. Athena integrates with Amazon QuickSight for easy visualization.
            </li>
            <li>
              To query staging data before loading into Redshift – You can stage your raw data in S3 before processing
              and loading it into Redshift, and then use Athena to query that data.
            </li>
            <li>
              Send AWS Service logs to S3 for Analysis with Athena – CloudTrail, Cloudfront, ELB/ALB and VPC flow logs
              can be analyzed Amazon Web Services – Big Data Analytics Options on AWS Page 43 of 56 with Athena.
              AWS CloudTrail logs include details about any API calls made to your AWS services, including from
              the console. CloudFront logs can be used to explore users’ surfing patterns across web properties
              served by CloudFront. Querying ELB/ALB logs allows you to see the source of traffic, latency, and
              bytes transferred to and from Elastic Load Balancing instances and backend applications. VPC flow
              logs capture information about the IP traffic going to and from network interfaces in VPCs in the
              Amazon VPC service. The logs allow you to investigate network traffic patterns and identify threats
              and risks across your VPC estate.
            </li>
            <li>
              Building Interactive Analytical Solutions with notebook-based solutions, e.g., RStudio, Jupyter, or
              Zeppelin - Data scientists and Analysts are often concerned about managing the infrastructure behind
              big data platforms while running notebook-based solutions such as RStudio, Jupyter, and Zeppelin.
              Amazon Athena makes it easy to analyze data using standard SQL without the need to manage infrastructure.
              Integrating these notebook-based solutions with Amazon Athena gives data scientists a powerful platform
              for building interactive analytical solutions.
            </li>
          </ul>
        </Typography>

        <Typography id={performance.id} variant={'title'}>
          {performance.display}
        </Typography>

        <Typography>
          You can improve the performance of your query by compressing, partitioning, and converting your data into
          columnar formats. Amazon Athena supports open source columnar data formats such as Apache Parquet and Apache
          ORC. Converting your data into a compressed, columnar format lowers your cost and improves query performance
          by enabling Athena to scan less data from S3 when executing your query.
        </Typography>

        <Typography id={scalabilityAndElasticity.id} variant={'title'}>
          {scalabilityAndElasticity.display}
        </Typography>

        <Typography>
          Athena is serverless, so there is no infrastructure to setup or manage, and you can start analyzing data
          immediately. Since it is serverless it can scale automatically, as needed.
        </Typography>

        <Typography id={referenceArchitecture.id} variant={'title'}>
          {referenceArchitecture.display}
        </Typography>

        <Typography>
          <img src={S3GlueAthenaArchitectureImg} />
        </Typography>

        <Typography>
          An example of creating an external table in AWS Athena, pointing to an AWS S3 Bucket:
          <Code>
            {athenaTableCreation}
          </Code>
        </Typography>



      </Fragment>
    )
  }
}

export default withStyles(styles)(AwsAthena)
