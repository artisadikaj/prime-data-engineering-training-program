/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import {Bold, Italic} from "presentations/Label";
import AmazonS3Img from 'assets/images/lecture6/amazon_s3.png'
import S3StasticsImg from 'assets/images/lecture6/s3_statistics.png'

const styles = ({ typography }) => ({
  root: {},
})


class AwsS3 extends React.Component {
  render() {
    const { classes, section } = this.props
    const whatIsS3 = section.children[0]
    const versioning = section.children[1]
    const encryption = section.children[2]
    const encryptionSSL = section.children[3]
    const storageTiers = section.children[4]
    const generalRules = section.children[5]
    const lifecycleRules = section.children[6]



    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography id={whatIsS3.id} variant={'title'}>
          {whatIsS3.display}
        </Typography>

        <Typography>
          <img src={AmazonS3Img} />
        </Typography>

        <Typography>
          Amazon S3 has a simple web services interface that you can use to store and retrieve any amount of data,
          at any time, from anywhere on the web.
        </Typography>

        <Typography>
          Amazon S3 is a cloud storage used to store objects (files) in “buckets” (directories)
          <ul>
            <li>
              <Bold>Buckets</Bold>
              <ul>
                <li>Buckets must have a globally unique name</li>
                <li>Buckets are defined at the region level</li>
                <li>
                  Naming convention:
                  <ul>
                    <li>No uppercase</li>
                    <li>No underscore</li>
                    <li>3-63 characters long</li>
                    <li>Not an IP</li>
                    <li>Must start with lowercase letter or number</li>
                  </ul>
                </li>

              </ul>
            </li>
            <li>
              <Bold>Objects</Bold>
              <ul>
                <li>
                  Objects (files) have a Key.The key is the FULL path:
                  <ul>
                    <li>/my_file.txt</li>
                    <li>/my_folder1/another_folder/my_file.txt</li>
                  </ul>
                </li>
                <li>There’s no concept of “directories” within buckets (although the UI will trick you to think otherwise)</li>
                <li>Just keys with very long names that contain slashes (“/”)</li>
                <li>
                  Object Values are the content of the body:
                  <ul>
                    <li>Max Size is 5TB</li>
                    <li>If uploading more than 5GB, must use “multipart upload”</li>
                  </ul>
                </li>
                <li>Metadata (list of text key / value pairs – system or user metadata)</li>
                <li>Tags (Unicode key / value pair – up to 10) – useful for security / lifecycle</li>
                <li>Version ID (if versioning is enabled)</li>
              </ul>
            </li>
          </ul>
        </Typography>

        <Typography id={versioning.id} variant={'title'}>
          {versioning.display}
        </Typography>

        <Typography>
          <ul>
            <li>You can version your files in AWS S3</li>
            <li>It is enabled at the bucket level</li>
            <li>Same key overwrite will increment the “version”: 1, 2, 3....</li>
            <li>
              <ul>
                It is best practice to version your buckets
                <li>Protect against unintended deletes (ability to restore a version)</li>
                <li>Easy roll back to previous version</li>
              </ul>
            </li>
            <li>Any file that is not versioned prior to enabling versioning will have version “null”</li>
          </ul>
        </Typography>

        <Typography id={encryption.id} variant={'title'}>
          {encryption.display}
        </Typography>

        <Typography>
          There are 4 methods of encrypting objects in S3
          <ul>
            <li>SSE-S3: encrypts S3 objects using keys handled & managed by AWS</li>
            <li>SSE-KMS: leverage AWS Key Management Service to manage encryption keys</li>
            <li>SSE-C: when you want to manage your own encryption keys</li>
            <li>Client Side Encryption</li>
          </ul>
        </Typography>

        <Typography id={encryptionSSL.id} variant={'title'}>
          {encryptionSSL.display}
        </Typography>

        <Typography>
          <ul>
            <li>
              AWS S3 exposes:
              <ul>
                <li>HTTP endpoint: non encrypted</li>
                <li>HTTPS endpoint: encryption in flight</li>
              </ul>
            </li>
            <li>You’re free to use the endpoint you want, but HTTPS is recommended</li>
            <li>HTTPS is mandatory for SSE-C</li>
            <li>Encryption in-transit utilizes SSL/TLS</li>
          </ul>
        </Typography>

        <Typography id={storageTiers.id} variant={'title'}>
          {storageTiers.display}
        </Typography>

        <Typography>
          <ul>
            <li>Amazon S3 Standard - General Purpose</li>
            <li>Amazon S3 Standard-Infrequent Access (IA)</li>
            <li>Amazon S3 One Zone-Infrequent Access</li>
            <li>Amazon S3 Reduced Redundancy Storage (deprecated)</li>
            <li>Amazon S3 Intelligent Tiering (new!)</li>
            <li>Amazon Glacier</li>
          </ul>
        </Typography>

        <Typography id={generalRules.id} variant={'title'}>
          {generalRules.display}
        </Typography>

        <Typography>
          <ul>
            <li>High durability (99.999999999%) of objects across multiple AZ</li>
            <li>If you store 10,000,000 objects with Amazon S3, you can on average expect to incur a loss of a
              single object once every 10,000 years</li>
            <li>99.99% Availability over a given year</li>
            <li>Sustain 2 concurrent facility failures</li>
            <li>Use Cases: Big Data analytics, mobile & gaming applications, content distribution...</li>
          </ul>
        </Typography>

        <Typography id={lifecycleRules.id} variant={'title'}>
          {lifecycleRules.display}
        </Typography>

        <Typography>
          <ul>
            <li>
              Set of rules to move data between different tiers, to save storage cost
            </li>
            <li>
              Example: General Purpose => Infrequent Access => Glacier
            </li>
            <li>
              Transition actions: It defines when objects are transitioned to another storage class.
              <ul>
                <li>
                  Eg:We can choose to move objects to Standard IA class 60 days after you created them or can move to
                  Glacier for archiving after 6 months
                </li>
              </ul>
            </li>
            <li>
              Expiration actions: Helps to configure objects to expire after a certain time period.
              S3 deletes expired objects on our behalf
              <ul>
                <li>
                  Eg: Access log files can be set to delete after a specified period of time
                </li>
              </ul>
            </li>
            <li>
              Can be used to delete incomplete multi-part uploads!
            </li>
          </ul>
        </Typography>

        <Typography>
          <img src={S3StasticsImg} style={{maxWidth: 720}}/>
        </Typography>

      </Fragment>
    )
  }
}

export default withStyles(styles)(AwsS3)
