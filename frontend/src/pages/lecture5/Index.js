/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import React from "react";
import Intro from "pages/lecture5/Intro";
import CloudComputing from "pages/lecture5/CloudComputing";
import AWSCloudProvider from "pages/lecture5/AWSCloudProvider";
import EC2 from "pages/lecture5/EC2";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_5.CLOUD_COMPUTING_INTRO:
        return <CloudComputing {...props} />
      case PAGES.LECTURE_5.AWS_CLOUD_PROVIDER:
        return <AWSCloudProvider {...props} />
      case PAGES.LECTURE_5.AWS_EC2:
        return <EC2 {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
