/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import React from "react";
import Intro from "pages/lecture6/Intro";
import EmrIntro from "pages/lecture7/EmrIntro";
import EmrArchitecture from "pages/lecture7/EmrArchitecture";
import EmrOtherFeatures from "pages/lecture7/EmrOtherFeatures";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_7.EMR_INTRO:
        return <EmrIntro {...props} />
      case PAGES.LECTURE_7.EMR_ARCHITECTURE:
        return <EmrArchitecture {...props} />
      case PAGES.LECTURE_7.EMR_OTHER_FEATURES:
        return <EmrOtherFeatures {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
