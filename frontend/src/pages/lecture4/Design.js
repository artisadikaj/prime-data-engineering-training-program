/**
 * Created by Labinot Vila on 29/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, {Fragment} from "react";
import {Bold} from "presentations/Label";
import NF1 from "assets/images/lecture4/1nf.png";
import NF2 from "assets/images/lecture4/2nf.png";
import NF3 from "assets/images/lecture4/3nf.png";
import PerformanceTuning from "assets/images/lecture4/performanceTuning.png";
import ExecutionOrder from "assets/images/lecture4/sqlExecutionOrder.png";

const styles = ({typography}) => ({
    root: {},
})

class Design extends React.Component {
    render() {
        const {classes, section} = this.props

        const dbDesign = section.children[0]
        const normalization = section.children[1]

        return (
            <Fragment>
                <Typography variant={'heading'}>
                    {section.display}
                    <Divider/>
                </Typography>

                <Typography>
                    Database designing is crucial to high performance database system.
                </Typography>
                <Typography>
                    Apart from performance improvement, properly designed database are easy to maintain, improve data
                    consistency and are cost effective in terms of disk storage space.
                </Typography>

                <Typography id={dbDesign.id} variant={'title'}>
                    {dbDesign.display}
                </Typography>

                <Typography>
                    <ul>
                        <li>
                            <Bold>Logical model</Bold>: This stage is concerned with developing a database model
                            based on requirements. The entire design is on paper without any physical implementations or
                            specific DBMS considerations.
                        </li>
                        <li>
                            <Bold>Physical model</Bold>: This stage implements the logical model of the database taking
                            into
                            account the DBMS and physical implementation factors.
                        </li>
                    </ul>
                </Typography>

                <Typography>
                    Designing Techniques: Normalization and ER Modeling
                </Typography>

                <Typography id={normalization.id} variant={'title'}>
                    {normalization.display}
                </Typography>

                <Typography>
                    Normalization is the process of organizing the fields and tables of a relational database to
                    minimize <Bold>redundancy</Bold> and <Bold>dependency</Bold>.
                </Typography>

                <Typography>
                    This can involve dividing larger tables into smaller tables and <Bold>defining
                    relationships</Bold> between
                    them.
                </Typography>

                <Typography>
                    <Bold>Common DB Design Mistakes:</Bold>

                    <ul>
                        <li>Tables with too many fields or with fields that do not relate to each other</li>
                        <li>Too many tables with similar data</li>
                        <li>Repeated rows</li>
                        <li>Using comma separated values or multiple values in a single row</li>
                        <li>Poor naming conventions</li>
                        <li>Poor or no planning</li>
                        <li>Non-Normalized data</li>
                    </ul>
                </Typography>

                <Typography>
                    Objective: Isolate data so that actions in a field can be made in one table and then propagated
                    through
                    the rest of the needed tables using properly defined relationships.
                </Typography>

                <Typography>
                    <Bold>Types of Normalizations:</Bold>

                    <ul>
                        <li>
                            <Typography>First Normal Form (1NF)</Typography>
                            <ul>
                                <li>No repeating or duplicate fields</li>
                                <li>Each row should contain only one value</li>
                                <li>Each row/record should be unique and identified by a primary key</li>
                            </ul>
                            <Typography>
                                <img style={{maxWidth: 1080}} src={NF1}/>
                            </Typography>
                        </li>

                        <li>
                            <Typography>Second Normal Form (2NF)</Typography>
                            <ul>
                                <li>Should be in 1NF</li>
                                <li>All non-key fields depend on all components of the primary key</li>
                                <li>No partial dependencies</li>
                            </ul>
                            <Typography>
                                <img style={{maxWidth: 1080}} src={NF2}/>
                            </Typography>
                        </li>

                        <li>
                            <Typography>Third Normal Form (3RD)</Typography>
                            <ul>
                                <li>Should be in 2NF</li>
                                <li>Has no transitive functional dependencies (A transitive functional dependency is
                                    when
                                    changing a non-key column, might cause any of the other non-key columns to change).
                                </li>
                            </ul>
                            <Typography>
                                <img style={{maxWidth: 1080}} src={NF3}/>
                            </Typography>
                        </li>
                    </ul>
                </Typography>

                <Typography>
                    <Bold>Functional Dependency</Bold>

                    <ul>
                        <li>Describes a relationship between columns within a single relation.</li>
                        <li>A column is dependent on another if one value can be used to determine the value of
                            another.
                        </li>
                    </ul>
                </Typography>

                <Typography>
                    Example: <Bold>first_name</Bold> is functionally dependent on id because id can be used to uniquely
                    determine the value of first name.
                </Typography>

              <Typography variant={'title'}>
                SQL Execution Order
              </Typography>
              <Typography>
                <img style={{maxWidth: 1080}} src={ExecutionOrder}/>
              </Typography>

              <Typography variant={'title'}>
                Performance Tuning
              </Typography>
              <Typography>
                <img style={{minWidth: 1080}} src={PerformanceTuning}/>
              </Typography>

            </Fragment>
        )
    }
}

export default withStyles(styles)(Design)
