/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import {Bold, Italic} from "presentations/Label";
import EmrClusterImg from 'assets/images/lecture7/emr_cluster.png'
import EmrStepSequenceImg from 'assets/images/lecture7/emr_step_sequence.png'
import EmrStepSequenceFailedImg from 'assets/images/lecture7/emr_step_sequence_failed.png'
import Code from "presentations/Code";

const styles = ({ typography }) => ({
  root: {},
})

const groupBy = `select first_name, count(*) from employees group by first_name;

select birth_date, count(*) from employees group by birth_date;

select salary, count(*) as sal_count from salaries group by salary order by sal_count desc;`

const groupByHaving = `select first_name, count(*) as emp_count from employees group by first_name;

select first_name, count(*) as emp_count from employees group by first_name having emp_count > 250;

select salary, count(*) as sal_count from salaries group by salary having sal_count > 100 order by sal_count asc;

-- The one with WHERE and HAVING
select salary, count(*) as sal_count 
from salaries 
where from_date > '1994-06-24'
group by salary 
having sal_count > 50
order by sal_count asc;`

const groupBySum = `select date_format(from_date, '%Y') from salaries;

select date_format(from_date, '%Y') as year, sum(salary) from salaries group by year;
`

const groupByMinMax = `select date_format(from_date, '%Y') from salaries;

select date_format(from_date, '%Y') as year, sum(salary) from salaries group by year;

-- The one with a SQL sub-query
SELECT 
    salary_year, max_salary, min_salary, max_salary - min_salary as delta
FROM
    (SELECT 
        DATE_FORMAT(from_date, '%Y') AS salary_year,
            MAX(salary) AS max_salary,
            MIN(salary) AS min_salary
    FROM
        salaries
    GROUP BY salary_year) as sub_select;`

const groupByAvg = `SELECT 
    DATE_FORMAT(from_date, '%Y') AS salary_year,
    AVG(salary) AS avg_salary,
    COUNT(*) AS total_recs,
    SUM(salary) AS salary_sum
FROM
    salaries
GROUP BY salary_year;


SELECT 
    avg_salary, salary_sum / total_recs AS calculated_avg
FROM
    (SELECT 
        DATE_FORMAT(from_date, '%Y') AS salary_year,
            AVG(salary) AS avg_salary,
            COUNT(*) AS total_recs,
            SUM(salary) AS salary_sum
    FROM
        salaries
    GROUP BY salary_year) AS subquery;`


class Aggregations extends React.Component {
  render() {
    const { classes, section } = this.props

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          <Bold>The SQL GROUP BY clause </Bold> groups rows that have the same values into summary rows, like
          "find the number of employees in each department".
        </Typography>

        <Typography>
          GROUP BY is often used with aggregate functions COUNT, MAX, MIN, SUM, AVG
        </Typography>

        <Typography>
          <Code>
            {groupBy}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL GROUP BY with HAVING clause
        </Typography>

        <Typography>
          The HAVING clause was added to SQL because the WHERE keyword could not be used with aggregate functions.
        </Typography>

        <Typography>
          <Code>
            {groupByHaving}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL GROUP BY with SUM() function
        </Typography>

        <Typography>
          SUM() function returns the total sum of a numeric column.
        </Typography>

        <Typography>
          <Code>
            {groupBySum}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL GROUP BY with MIN() & MAX() function
        </Typography>

        <Typography>
          The MIN() function returns the smallest value of the selected column.
          The MAX() function returns the largest value of the selected column.
        </Typography>

        <Typography>
          <Code>
            {groupByMinMax}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL GROUP BY with AVG() function
        </Typography>

        <Typography>
          The AVG() function returns the average value of a numeric column.
        </Typography>

        <Typography>
          <Code>
            {groupByAvg}
          </Code>
        </Typography>


      </Fragment>
    )
  }
}

export default withStyles(styles)(Aggregations)
