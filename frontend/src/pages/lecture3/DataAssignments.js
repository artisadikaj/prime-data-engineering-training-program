/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import PageLink from "presentations/rows/nav/PageLink";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import Exercise from 'presentations/Exercise'
import Code from 'presentations/Code'
import { Bold } from 'presentations/Label'

const styles = ({ typography }) => ({
  root: {},
})

const exercise1 = `In the following assignments, you will have to use these tables:

customers        ->  \`CustomerID\` STRING,    \`CustomerName\` STRING,  \`ContactName\` STRING,   \`Address\` STRING,
                     \`City\` STRING,          \`PostalCode\` STRING,    \`Country\` STRING

employees        ->  \`EmployeeID\` STRING,    \`LastName\` STRING,      \`FirstName\` STRING,     \`BirthDate\` STRING,
                     \`Photo\` STRING,         \`Notes\` STRING

orders           ->  \`OrderID\` STRING,       \`CustomerID\` STRING,    \`EmployeeID\` STRING,    \`OrderDate\` STRING,
                     \`ShipperID\` STRING

order_details    ->  \`OrderDetailID\` STRING, \`OrderID\` STRING,       \`ProductID\` STRING,     \`Quantity\` STRING

products         ->  \`ProductID\` STRING,     \`ProductName\` STRING,   \`SupplierID\` STRING,    \`CategoryID\` STRING,
                     \`Unit\` STRING,\`Price\` STRING

To use a dataset, you can use provider.getDatasets().get(dataset) where \`dataset\` can be the names explained above.`
const exercise4 = `id,date
1,2022-02-01
1,2022-02-02
1,2022-02-03
1,2022-02-05
1,2022-02-06
2,2022-02-01
2,2022-02-02

Should output the 'name', 'contact name' and '3' of id = 1, because id = 1 ordered 3 days in a row, then two, while id = 2 
ordered only two days in a row`
const exercise5 = `id,date
1,2022-02-01
1,2022-02-02
1,2022-02-03
1,2022-02-05
1,2022-02-06

Should give 1,1 (CustomerID = 1, days = 1), since on 2022-02-04, no order was made; hint: use "sequence" and "explode"`
const quiz = `1. Apache Spark is a unified analytics engine, unified means that there is no need for third party libraries, since it already contains a lot of analytics components.

2. Apache Spark, which is written in Java, differentiates from other data processing frameworks because it does all the processing in memory, not in disk.

3. Apache Spark is lazy, meaning that it first constructs a plan, then optimizes it and finally runs it.

4. Apache Spark manifests itself in the form of SparkContext.

5. Running Apache Spark in standalone mode utilizes local computer's cores for parallel processing.

6. Apache Spark supports only two variable types: Broadcast variables and Accumulators.

7. Apache Spark datasets are mutable, therefore we can do transformations without overwriting the variable.

8. Transformations return a value to the driver program after running actions.

9. Cache operator is a transformation.

10. Apache Spark can read both structured and unstructured data.`
class SparkSQL extends React.Component {
  render() {
    const { section } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography>
          <Code>{exercise1}</Code>
        </Typography>
        <Typography>
          <Bold>Assignment 1</Bold>: Show full name (`FirstName` and `LastName`) of the eldest employee
        </Typography>
        <Exercise url={'/assignments/lecture3/assignment1'} />

        <Typography>
          <Bold>Assignment 2</Bold>: Find the categories (`CategoryID`, `SupplierID`, `Amount`) that have sales of over
          50.000, excluding sales supplier `SupplierId` = 1. (`Quantity` * `Price` > 50.000)
        </Typography>
        <Exercise url={'/assignments/lecture3/assignment2'} />

        <Typography>
          <Bold>Assignment 3</Bold>: Find the top 10 customers (`CustomerName`, `Address` and `City`) who have ordered the most
        </Typography>
        <Exercise url={'/assignments/lecture3/assignment3'} />

        <Typography>
          <Bold>Assignment 4</Bold>: Find the customers (`CustomerName`, `ContactName`, `NrOfDays`) that have ordered the most in consecutive days
        </Typography>
        <Typography>
          <Code>{exercise4}</Code>
        </Typography>
        <Exercise url={'/assignments/lecture3/assignment4'} />

        <Typography>
          <Bold>Assignment 5</Bold>: Find all the days that the customer did not do an order from the first date to the
          last that he purchased (`CustomerID` and `days`). For example:
        </Typography>
        <Typography>
          <Code>{exercise5}</Code>
        </Typography>
        <Exercise url={'/assignments/lecture3/assignment5'} />

        <Typography>
          <Bold>General Spark Knowledge</Bold>: Answer the following questions with True or False:
        </Typography>
        <Typography>
          <Code>{quiz}</Code>
        </Typography>
        <Typography>
          Answers:
        </Typography>
        <Exercise url={'/assignments/lecture3/assignment6'} />
      </Fragment>
    )
  }
}

export default withStyles(styles)(SparkSQL)
