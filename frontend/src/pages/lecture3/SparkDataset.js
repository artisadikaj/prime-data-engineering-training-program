/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import PageLink from "presentations/rows/nav/PageLink";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import Code from 'presentations/Code'
import { Bold } from 'presentations/Label'
import Exercise from 'presentations/Exercise'

const styles = ({ typography }) => ({
  root: {},
})
const sparkSession = `import org.apache.spark.sql.SparkSession;

SparkSession spark = SparkSession
  .builder()
  .appName("Java Spark SQL basic example")
  .config("spark.some.config.option", "some-value")
  .getOrCreate();`
const createDataset = `import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
....

Dataset<Row> df = spark.read().json("src/main/resources/people.json");`

const primitives = `long column1 = row.getLong(1);
boolean column2 = row.getBoolean(2);`

const datasetOperations = `// Print the schema in a tree format
df.printSchema();

// Select only the "name" column
df.select("name").show();

// Select everybody, but increment the age by 1
df.select(col("name"), col("age").plus(1)).show();

// Select people older than 21
df.filter(col("age").gt(21)).show();

// Count people by age
df.groupBy("age").count().show();

// To create Dataset<Row> using SparkSession
Dataset<Row> people = spark.read().csv("...");
Dataset<Row> department = spark.read().csv("...");

// Filter, Join and aggregate two datasets
people.filter(people.col("age").gt(30))
 .join(department, people.col("deptId").equalTo(department.col("id")))
 .groupBy(department.col("name"), people.col("gender"))
 .agg(avg(people.col("salary")), max(people.col("age")));


// Assign a value to a column bassed on a condition expression
Dataset<Row> result = students.withColumn("ExamStatus", when(col("grade").equalTo("F"), "Failed").otherwise(lit("Passed")));`

class SparkDataset extends React.Component {
  render() {
    const { classes, section } = this.props
    const sql = section.children[0]
    const datasets = section.children[1]
    const session = section.children[2]
    const dataframes = section.children[3]
    const row = section.children[4]
    const operations = section.children[5]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography>
          Spark SQL is a Spark module for structured data processing. Unlike the basic Spark RDD API, the interfaces provided by Spark SQL provide Spark with more information about the structure of both the data and the computation being performed.
        </Typography>
        <Typography>
          Internally, Spark SQL uses this extra information to perform extra optimizations. There are several ways to interact with Spark SQL including SQL and the Dataset API.
        </Typography>
        <Typography>
          When computing a result the same execution engine is used, independent of which API/language you are using to express the computation. This unification means that developers can easily switch back and forth between different APIs based on which provides the most natural way to express a given transformation.
        </Typography>
        <Typography id={sql.id} variant={'title'}>
          {sql.display}
        </Typography>
        <Typography>
          One use of Spark SQL is to execute SQL queries. Spark SQL can also be used to read data from an existing Hive installation. When running SQL from within another programming language the results will be returned as a Dataset/DataFrame.
        </Typography>
        <Typography id={datasets.id} variant={'title'}>
          {datasets.display}
        </Typography>
        <Typography>
          A Dataset is a distributed collection of data. Dataset is a new interface added in Spark 1.6 that provides the benefits of RDDs (strong typing, ability to use powerful lambda functions) with the benefits of Spark SQL’s optimized execution engine.
        </Typography>
        <Typography>
          The Dataset API is available in Scala and Java. Python does not have the support for the Dataset API.
        </Typography>
        <Typography>
          A DataFrame is a Dataset organized into named columns. It is conceptually equivalent to a table in a relational database or a data frame in R/Python, but with richer optimizations under the hood
        </Typography>
        <Typography>
          In Java API, users need to use Dataset to represent a DataFrame.
        </Typography>
        <Typography id={session.id} variant={'title'}>
          {session.display}
        </Typography>
        <Typography>
          The entry point into all functionality in Spark is the SparkSession class. To create a basic SparkSession, just use SparkSession.builder():
          <Code>
            {sparkSession}
          </Code>
          SparkSession in Spark 2.0 provides builtin support for Hive features including the ability to write queries using HiveQL, access to Hive UDFs, and the ability to read data from Hive tables.
        </Typography>
        <Typography id={dataframes.id} variant={'title'}>
          {dataframes.display}
        </Typography>
        <Typography>
          With a SparkSession, applications can create DataFrames from an existing RDD, from a Hive table, or from Spark data sources.
        </Typography>
        <Typography>
          As an example, the following creates a DataFrame based on the content of a JSON file:
          <Code>
            {createDataset}
          </Code>
          <ul>
            <li>Dataset takes on two distinct APIs characteristics: a strongly-typed API and an untyped API.</li>
            <li>Consider DataFrame as untyped view of a Dataset, which is a Dataset of Row where a Row is a generic untyped JVM object.</li>
          </ul>
        </Typography>
        <Typography id={row.id} variant={'title'}>
          {row.display}
        </Typography>
        <Typography>
          <ul>
            <li>
              Row objects represent records inside dataset and are simply fixed- length arrays of fields.
            </li>
            <li>
              Row objects have a number of getter functions to obtain the value of each field given its index. The get method takes a column number and returns us an object type; we are responsible for casting the object to the correct type.
              <Code>
                {`Object column5 = row.get(5);`}
              </Code>
            </li>
            <li>
              For primitive and boxed types, there is a get type method, which returns the value of that type.
              <Code>
                {primitives}
              </Code>
            </li>
          </ul>
        </Typography>
        <Typography id={operations.id} variant={'title'}>
          {operations.display}
        </Typography>
        <Typography>
          DataFrames provide a domain-specific language for structured data manipulation in Scala, Java, Python and R.
          <Code>
            {datasetOperations}
          </Code>
          <Bold>Exercise 5</Bold>: Read from employees and print the schema, find the employee with the highest salary, also find the total salary given to all employees.
          Show the results in the console. More information on how to read this dataset can be found within the source code.
          <Exercise url={'/lecture3/exercise5'} />
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(SparkDataset)
