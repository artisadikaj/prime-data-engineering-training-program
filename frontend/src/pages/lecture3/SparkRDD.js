/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import Code from 'presentations/Code'
import { Bold, Italic } from 'presentations/Label'
import MapReduceStagesImage from 'assets/images/lecture3/map_reduce.png'
import Exercise from 'presentations/Exercise'

const styles = ({ typography }) => ({
  root: {},
  results: {
    width: '100%',
    height: 320,
    overflowY: 'auto',
    position: 'relative'
  }
})

const javaSpark = `import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.SparkConf;

SparkConf conf = new SparkConf().setAppName(appName).setMaster(master);
JavaSparkContext sc = new JavaSparkContext(conf);`

const paralelize = `List<Integer> data = Arrays.asList(1, 2, 3, 4, 5);
JavaRDD<Integer> distData = sc.parallelize(data);`
const fileRead = `JavaRDD<String> distFile = sc.textFile("data.txt");`

const SparkRDD = (props) => {
  const { classes, section } = props
  const sparkWithJava = section.children[0]
  const rdds = section.children[1]
  const operations = section.children[2]
  return (
    <Fragment>
      <Typography variant={'heading'}>
        {section.display}
        <Divider />
      </Typography>
      <Typography>
        The main abstraction Spark provides is a resilient distributed dataset (RDD), which is a collection of elements partitioned across the nodes of the cluster that can be operated on in parallel.
      </Typography>
      <Typography>
        A second abstraction in Spark is shared variables that can be used in parallel operations. By default, when Spark runs a function in parallel as a set of tasks on different nodes, it ships a copy of each variable used in the function to each task.
      </Typography>
      <Typography>
        Sometimes, a variable needs to be shared across tasks, or between tasks and the driver program.
      </Typography>
      <Typography>
        Spark supports two types of shared variables:
        <ul>
          <li>Broadcast variables - which can be used to cache a value in memory on all nodes, and</li>
          <li>Accumulators, which are variables that are only “added” to, such as counters and sums.</li>
        </ul>
      </Typography>
      <Typography id={sparkWithJava.id} variant={'title'}>
        {sparkWithJava.display}
      </Typography>
      <Typography>
        Throughout this course on Apache Spark, we'll be using Java to develop data processing applications.
      </Typography>
      <Typography>
        The first thing a Spark program must do is to create a JavaSparkContext object, which tells Spark how to access a cluster. To create a SparkContext you first need to build a SparkConf object that contains information about your application.
        <Code>
          {javaSpark}
        </Code>
        The appName parameter is a name for your application to show on the cluster UI. master is a Spark, Mesos or YARN cluster URL, or a special “local” string to run in local mode
      </Typography>
      <Typography id={rdds.id} variant={'title'}>
        {rdds.display}
      </Typography>
      <Typography>
        There are two ways to create RDDs:
        <ol>
          <li>Parallelized collection in your driver program,<Code>{paralelize}</Code></li>
          <li>External storage system, such as a shared filesystem, HDFS, or Amazon S3.<Code>{fileRead}</Code></li>
        </ol>
        There are other data sources which can be integrated with Spark and used to create RDDs including JDBC, Cassandra, and Elastisearch, etc.
      </Typography>
      <Typography variant={'section'}>
        Characteristics of RDDs
      </Typography>
      <Typography>
        <ul>
          <li>
            RDDs are Distributed
            <ul>
              <li>Each RDD is broken into multiple pieces called partitions, and these partitions are divided across the clusters.</li>
            </ul>
          </li>
          <li>
            RDDs are Immutable
            <ul>
              <li>They cannot be changed after they are created.</li>
              <li>Immutability makes it safe to share RDDs across processes.</li>
            </ul>
          </li>
          <li>
            RDDs are Resilient
            <ul>
              <li>RDDs are a deterministic function of their input. This plus immutability also means the RDDs parts can be recreated at any time.</li>
              <li>In case of any node in the cluster goes down, Spark can recover the parts of the RDDs from the input and pick up from where it left off.</li>
            </ul>
          </li>
        </ul>
      </Typography>
      <Typography id={operations.id} variant={'title'}>
        {operations.display}
      </Typography>
      <Typography>
        RDDs support two types of operations:
        <ul>
          <li><Bold>Transformations</Bold> - which create a new dataset from an existing one, and</li>
          <li><Bold>Actions</Bold> - which return a value to the driver program after running a computation on the dataset.</li>
        </ul>
        All transformations in Spark are <Bold>lazy</Bold>, in that they do not compute their results right away. Instead, they just remember the transformations applied to some base dataset (e.g. a file). The transformations are only computed when an action requires a result to be returned to the driver program.
      </Typography>
      <Typography variant={'section'}>
        Transformations
      </Typography>
      <Typography>
        The two most common transformations are filter(), map() and reduce()
        <ul>
          <li>
            <Bold>filter(func)</Bold> - Takes in a function and returns an RDD formed by selecting those elements which pass the filter function.
            <Code>{`JavaRDD<String> airportsInUSA = airports.filter(line -> line.split(",")[3].equals("\\"United States\\""));`}</Code>
          </li>
          <li>
            <Bold>map(func)</Bold> - Takes in a function and passes each element in the input RDD through the function, with the result of the function being the new value of each element in the resulting RDD.
            <Code>{`JavaRDD<String> lowerCaseLines = lines.map(line -> line.toUpperCase());`}</Code>
          </li>
          <li>
            <Bold>flatMap(func)</Bold> - Similar to map, but each input item can be mapped to 0 or more output items (so func should return a Seq rather than a single item).
            <Code>{`JavaRDD<String> words = lines.flatMap(s -> Arrays.asList(s.split(" ")).iterator());`}</Code>
          </li>
        </ul>
        <Bold>Exercise 1</Bold>: Create a Spark program to read the airport data from data/s3.spark.rdd.api/airport/airports.txt, find all the
        * airports which are located in the United States.
      </Typography>
      <Exercise url={'/lecture3/exercise1'} />
      <Typography variant={'section'}>
        Set operations which are performed on one RDD
      </Typography>
      <Typography>
        <ul>
          <li><Bold>sample</Bold> - The sample operation will create a random sample from an RDD</li>
          <li><Bold>distinct</Bold> - The distinct transformation returns the distinct rows from the input RDD.</li>
        </ul>
      </Typography>
      <Typography variant={'section'}>
        Set operations which are performed on two RDDs
      </Typography>
      <Typography>
        <ul>
          <li>
            <Bold>union</Bold> - Union operation gives us back an RDD consisting of the data from both input RDDs.
            <Code>{`rdd1 = rdd1.union(rdd2);`}</Code>
          </li>
          <li>
            <Bold>intersection</Bold> - Intersection operation returns the common elements which appear in both input RDDs.
            <Code>{`rdd1 = rdd1.intersection(rdd2);`}</Code>
          </li>
          <li>
            <Bold>subtract</Bold> - Subtract operation takes in another RDD as an argument and returns us an RDD that only contains element present in the first RDD and not the second RDD.
            <Code>{`rdd1 = rdd1.subtract(rdd2);`}</Code>
          </li>
          <li>
            <Bold>cartesian product</Bold> - Cartesian transformation returns all possible pairs of a and b where a is in the source RDD and b is in the other RDD.
            <Code>{`rdd1 = rdd1.cartesian(rdd2);`}</Code>
          </li>
        </ul>
        <Bold>Exercise 2</Bold>: Create two RDDs and apply union, intersection, and subtract operations.
      </Typography>
      <Exercise url={'/lecture3/exercise2'} />
      <Typography variant={'section'}>
        RDD Actions
      </Typography>
      <Typography>
        Actions are the operations which will return a final value to the driver program or persist data to an external storage system.
      </Typography>
      <Typography>
        <ul>
          <li>
            <Bold>collect()</Bold> - Return all the elements of the dataset as an array at the driver program.
            <Code>{`List<String> words = wordRdd.collect();`}</Code>
          </li>
          <li>
            <Bold>count()</Bold> - Return the number of elements in the dataset.
            <Code>{`wordRdd.count();`}</Code>
          </li>
          <li>
            <Bold>countByValue()</Bold> - Will look at unique values in the each row of your RDD and return a map of each unique value to its count.
            <Code>{`Map<String, Long> wordCounts = words.countByValue();`}</Code>
          </li>
          <li>
            <Bold>take()</Bold> - take action takes n elements from an RDD.
            <Code>{`List<String> words = wordRdd.take(3);`}</Code>
          </li>
          <li>
            <Bold>saveAsTextFile()</Bold> - Can be used to write data out to a distributed storage system such as HDFS or Amazon S3, or even local file system.
            <Code>{`airportsRdd.saveAsTextFile("out/transformed_airport_data.txt");`}</Code>
          </li>
          <li>
            <Bold>reduce(func)</Bold> - Aggregate the elements of the dataset using a function func (which takes two arguments and returns one).
            <Code>{`Integer product = integerRdd.reduce((x, y) -> x * y);`}</Code>
          </li>
        </ul>
        <Bold>Exercise 3</Bold>: Create a Spark program to read the first 100 prime numbers from data/s3.spark.rdd.api/prime_nums/prime_nums.txt, print the sum of those numbers to console. Each row of the input file contains 10 prime numbers separated by spaces.
      </Typography>
      <Exercise url={'/lecture3/exercise3'} />
      <Typography>
        <Bold>Exercise 4</Bold>: Read the words.txt file using sc.textFile("data/s3.spark.rdd.api/words/words.txt"); and count how many times a word repeats.
      </Typography>
      <Typography>
        And the result:
      </Typography>
      <Exercise url={'/lecture3/exercise4'} />
      <Typography variant={'section'}>
        Background stages
      </Typography>
      <img src={MapReduceStagesImage}/>
    </Fragment>
  )
}

export default withStyles(styles)(SparkRDD)
