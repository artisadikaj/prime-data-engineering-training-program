/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SparkFeaturesImage from 'assets/images/lecture3/spark_features.png'
import SparkComponentImage from 'assets/images/lecture3/spark_components.png'
import SparkImage from 'assets/images/lecture3/spark.png'
import Code from 'presentations/Code'

const styles = ({ typography }) => ({
  root: {}
})

const submit = `# Run application locally on 8 cores
./bin/spark-submit \\
  --class org.apache.spark.examples.SparkPi \\
  --master local[8] \\
  /path/to/examples.jar \\
  100

# Run on a Spark standalone cluster in client deploy mode
./bin/spark-submit \\
  --class org.apache.spark.examples.SparkPi \\
  --master spark://207.184.161.138:7077 \\
  --executor-memory 20G \\
  --total-executor-cores 100 \\
  /path/to/examples.jar \\
  1000

# Run on a Spark standalone cluster in cluster deploy mode with supervise
./bin/spark-submit \\
  --class org.apache.spark.examples.SparkPi \\
  --master spark://207.184.161.138:7077 \\
  --deploy-mode cluster \\
  --supervise \\
  --executor-memory 20G \\
  --total-executor-cores 100 \\
  /path/to/examples.jar \\
  1000

# Run on a YARN cluster
export HADOOP_CONF_DIR=XXX
./bin/spark-submit \\
  --class org.apache.spark.examples.SparkPi \\
  --master yarn \\
  --deploy-mode cluster \\  # can be client for client mode
  --executor-memory 20G \\
  --num-executors 50 \\
  /path/to/examples.jar \\
  1000

# Run a Python application on a Spark standalone cluster
./bin/spark-submit \\
  --master spark://207.184.161.138:7077 \\
  examples/src/main/python/pi.py \\
  1000

# Run on a Mesos cluster in cluster deploy mode with supervise
./bin/spark-submit \\
  --class org.apache.spark.examples.SparkPi \\
  --master mesos://207.184.161.138:7077 \\
  --deploy-mode cluster \\
  --supervise \\
  --executor-memory 20G \\
  --total-executor-cores 100 \\
  http://path/to/examples.jar \\
  1000

# Run on a Kubernetes cluster in cluster deploy mode
./bin/spark-submit \\
  --class org.apache.spark.examples.SparkPi \\
  --master k8s://xx.yy.zz.ww:443 \\
  --deploy-mode cluster \\
  --executor-memory 20G \\
  --num-executors 50 \\
  http://path/to/examples.jar \\
  1000`

const Spark = (props) => {
  const { classes, section } = props
  const components = section.children[0]
  const architecture = section.children[1]
  const clusters = section.children[2]
  const exercise = section.children[3]
  return (
    <Fragment>
      <Typography variant={'heading'}>
        {section.display}
        <Divider />
      </Typography>
      <Typography>
        Apache Spark is a unified analytics engine for large-scale data processing, which allows to efficiently execute batch, streaming, machine learning or SQL workloads.
      </Typography>
      <Typography variant={'section'}>
        Speed
      </Typography>
      <Typography>
        <ul>
          <li>Run computations in memory.</li>
          <li>Apache Spark has an advanced DAG execution engine that supports acyclic data flow and in-memory computing.</li>
          <li>100 times faster in memory and 10 times faster even when running on disk than MapReduce.</li>
        </ul>
      </Typography>
      <Typography variant={'section'}>
        APIs
      </Typography>
      <Typography>
        Write applications quickly in Java, Scala, Python, R, and SQL.
      </Typography>
      <Typography>
        Spark offers over 80 high-level operators that make it easy to build parallel apps. And you can use it interactively from the Scala, Python, R, and SQL shells.
      </Typography>
      <Typography variant={'section'}>
        Generality
      </Typography>
      <Typography>
        <ul>
          <li>A general programming model that enables developers to write an application by composing arbitrary operators.</li>
          <li>Spark makes it easy to combine different processing models seamlessly in the same application.</li>
          <li>
            Example:
            <ul>
              <li>Data classification through Spark machine learning library.</li>
              <li>Streaming data through source via Spark Streaming.</li>
              <li>Querying the resulting data in real time through Spark SQL.</li>
            </ul>
          </li>
        </ul>
      </Typography>
      <img style={{ maxWidth: '100%' }} src={SparkFeaturesImage} />
      <Typography id={components.id} variant={'title'}>
        {components.display}
      </Typography>
      <img style={{ maxWidth: '100%' }} src={SparkComponentImage}/>
      <Typography variant={'section'}>
        Spark Core
      </Typography>
      <Typography>
        Spark Core is the underlying general execution engine for the Spark platform, all other functionality is built on top of it.
      </Typography>
      <Typography>
        Provides distributed task dispatching, scheduling, and basic IO functionalities, exposed through an application programming interface centered on the RDD, which is Spark’s primary programming abstraction.
      </Typography>
      <Typography variant={'section'}>
        Spark SQL
      </Typography>
      <Typography>
        <ul>
          <li>Spark package designed for working with structured data which is built on top of Spark Core.</li>
          <li>Provides an SQL-like interface for working with structured data.</li>
          <li>More and more Spark workflow is moving towards Spark SQL.</li>
        </ul>
      </Typography>
      <Typography variant={'section'}>
        Spark Streaming
      </Typography>
      <Typography>
        <ul>
          <li>Running on top of Spark, Spark Streaming provides an API for manipulating data streams that closely match the Spark Core’s RDD API.</li>
          <li>Enables powerful interactive and analytical applications across both streaming and historical data while inheriting Spark’s ease of use and fault tolerance characteristics.</li>
        </ul>
      </Typography>
      <Typography variant={'section'}>
        Spark MLlib
      </Typography>
      <Typography>
        <ul>
          <li>Built on top of Spark, MLlib is a scalable machine learning library that delivers both high-quality algorithms and blazing speed.</li>
          <li>Usable in Java, Scala, and Python as part of Spark applications.</li>
          <li>Consists of common learning algorithms and utilities, including classification, regression, clustering, collaborative filtering and dimensionality reduction, etc.</li>
        </ul>
      </Typography>
      <Typography variant={'section'}>
        Spark - GraphX
      </Typography>
      <Typography>
        <ul>
          <li>A graph computation engine built on top of Spark that enables users to interactively create, transform and reason about graph structured data at scale.</li>
          <li>Extends the Spark RDD by introducing a new Graph abstraction: a directed multigraph with properties attached to each vertex and edge.</li>
        </ul>
      </Typography>
      <Typography id={architecture.id} variant={'title'}>
        {architecture.display}
      </Typography>

      <Typography>
        Spark applications run as independent sets of processes on a cluster, coordinated by the SparkContext object in your main program (called the driver program).
      </Typography>
      <Typography>
        At a high level, every Spark application consists of a driver program that runs the user’s main function and executes various parallel operations on a cluster.
      </Typography>
      <img src={SparkImage}/>
      <Typography>
        There are several useful things to note about this architecture:
        <ul>
          <li>
            Each application gets its own executor processes, which stay up for the duration of the whole application and run tasks in multiple threads. This has the benefit of isolating applications from each other, on both the scheduling side (each driver schedules its own tasks) and executor side (tasks from different applications run in different JVMs). However, it also means that data cannot be shared across different Spark applications (instances of SparkContext) without writing it to an external storage system.
          </li>
          <li>
            Spark is agnostic to the underlying cluster manager. As long as it can acquire executor processes, and these communicate with each other, it is relatively easy to run it even on a cluster manager that also supports other applications (e.g. Mesos/YARN).
          </li>
          <li>
            The driver program must listen for and accept incoming connections from its executors throughout its lifetime (e.g., see spark.driver.port in the network config section). As such, the driver program must be network addressable from the worker nodes.
          </li>
          <li>
            Because the driver schedules tasks on the cluster, it should be run close to the worker nodes, preferably on the same local area network. If you’d like to send requests to the cluster remotely, it’s better to open an RPC to the driver and have it submit operations from nearby than to run a driver far away from the worker nodes.
          </li>
        </ul>
      </Typography>
      <Typography id={clusters.id} variant={'title'}>
        {clusters.display}
      </Typography>

      <Typography>
        <ul>
          <li>Standalone – a simple cluster manager included with Spark that makes it easy to set up a cluster.</li>
          <li>Apache Mesos – a general cluster manager that can also run Hadoop MapReduce and service applications.</li>
          <li>Hadoop YARN – the resource manager in Hadoop 2.</li>
          <li>Kubernetes – an open-source system for automating deployment, scaling, and management of containerized applications.</li>
        </ul>
      </Typography>
      <Typography variant={'section'}>
        Submitting job:
      </Typography>
      <Typography>
        Applications can be submitted to a cluster of any type using the spark-submit script:
        <Code>
          {submit}
        </Code>
      </Typography>
      <Typography variant={'section'}>
        Monitoring
      </Typography>
      <Typography>
        Each driver program has a web UI, typically on port 4040, that displays information about running tasks, executors, and storage usage. Simply go to http://:4040 in a web browser to access this UI.
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(Spark)
