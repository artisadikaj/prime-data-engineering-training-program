/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import Chart from "presentations/Chart";
import SimpleLink from "presentations/rows/SimpleLink";
import { Normal } from "presentations/Label";
import LoadingIndicator from 'presentations/LoadingIndicator'
import _ from 'underscore'
import Button from '@material-ui/core/Button'
import withPostRequestTests from 'middleware/withPostRequestTests'

import {
  randomValuesOfLength,
  randomPositiveValues,
  randomWordsOfLength,
  randomGroupsOfLength
} from 'utils/DataGenerator'
import ErrorBox from "presentations/ErrorBox";



const styles = ({ typography, size }) => ({
  root: {},
  graphs: {
    display: 'flex',
    flexFlow: 'row wrap',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    width: '100%'
  },
  title: {
    zIndex: 10,
  },
  card: {
    backgroundColor: 'white',
    width: `calc(32% - ${size.spacing * 2}px)`,
    margin: size.spacing,
    height: 420,
    padding: 8,
    display: 'flex',
    flexFlow: 'column wrap',
    position: 'relative',
    alignItems: 'flex-start'
  },
  graph: {
    display: 'flex',
    flex: 1,
    width: '100%',
    height: 'auto'
  }
})


/**
 * Returns given a length, an array that contains that many elements of the form
 * {
 *  name: 'Random Word',
 *  group: 'Random Group',
 *  value: 'Random Value'
 * }
 * @param {int} length
 * @param {boolean} positive = false
 */
const randomCategoryData = (length, positive = false) => {
  const values = positive ? randomPositiveValues(length) : randomValuesOfLength(length)
  const words = randomWordsOfLength(length)
  const groups = randomGroupsOfLength(length)
  return words.map((next, index) => { return { name: next, group: groups[index], value: values[index]} })
}

const Card = ({inputs = [], onSuccess, onCompare, url, title, titleClass, options, graphClass, ...other}) => {
  const { response, error, isLoading, runTests } = withPostRequestTests(inputs, url, onCompare, onSuccess)
  return <div {...other}>
    <Typography variant={'title'} className={titleClass}>{title}</Typography>
    <LoadingIndicator show={isLoading} />
    {!!error && <ErrorBox message={error.message} onRetryClicked={runTests} />}
    <Chart className={graphClass} options={response} />
  </div>
}

const axisGraphDefaultOptions = {
  xAxis: {
    type: 'value'
  },
  yAxis: {
    type: 'value'
  }
}
const GRAPH_TYPES = {
  LINE: 'line',
  SCATTER: 'scatter',
  BAR: 'bar',
  PIE: 'pie',
  TREEMAP: 'treemap'
}


const BinarySearch = () => {
  const values = [1, 4, 12, 16, 22, 24, 28, 44, 70]
  const inputs = [{
    values,
    search: 4,
  }, {
    values,
    search: 3,
  }, {
    values,
    search: 28,
  }, {
    values,
    search: 70,
  }]
  const onCompare = (input, output) => {
    console.log('on compare', output === input.values.indexOf(input.search))
    return output === input.values.indexOf(input.search)
  }
  const onSuccess = (input, output) => {
    return output
  }
  const { response, error, isLoading, runTests } = withPostRequestTests(inputs, '/assignments/lecture2/binarySearch', onCompare, onSuccess)
  const isCorrect = !error && response !== null
  return (
    <React.Fragment>
      <Typography style={{ position: 'relative' }} variant='p'>
        For the given values: {inputs[0].values.join(', ')}, return the index of the values {inputs.map(next => next.search).join(', ')} which is {inputs.map(next => values.indexOf(next.search)).join(', ')} using Binary Search!<br/>
        Currently the solution is: <Normal style={{ color: isCorrect ? 'green' : 'red' }}>{isCorrect ? 'Correct' : 'Not Correct'}</Normal><br/>
        <LoadingIndicator show={isLoading}/>
      </Typography>
      <Button disabled={isLoading} onClick={runTests}>Retry</Button>
    </React.Fragment>
  )
}


class JavaAssignments extends React.Component {

  /**
   * Function as Line: y = x * 2
   */
  function1 = (props) => {
    const inputs = [
      [48, -50, 28, -27, -46, 4, 46, -49, 25, -25, -32, -16, 44, 20, -26, 11, 19, -48, 13, 22, 46, 8, 29, 5, -3, -27, 0, 11, 13, 36],
      [17, -12, -19, 37, -48, -26, -22, -39, 15, 15, -40, 34, -11, 6, -32, 43, 20, -46, -3, -18, 1, 23, -1, 45, 25, 14, 7, 6, 23, -24],
      [39, 4, 29, -38, 30, -14, -11, 28, -1, 41, 26, 15, -5, -1, 39, 35, -47, -2, 11, 28, -30, -30, 47, 39, -44, -44, -45, 38, -2, -36],
      [-14, 18, -36, 15, -33, -12, -40, -37, 29, -6, 28, 44, 15, 15, -18, -3, -45, -1, -23, -26, -6, 50, -33, -11, -6, -26, 47, 2, 12, 5],
      [8, 12, -38, 4, -15, 3, -23, 5, -22, 6, 23, -22, -11, -4, -49, -44, -6, -40, -45, 42, -25, -16, 16, 42, -45, 14, -37, -30, 46, -32],
      [],
      [14, 0, 0]
    ]
    const onCompare = (input, output) => {
      const result = input.map(x => x * 2)
      return _.isEqual(result, output)
    }
    const onSuccess = (input, output) => {
      return {
        ...axisGraphDefaultOptions,
        series: [
          {
            data: input.map((next, index) => [input[index], output[index]]),
            type: GRAPH_TYPES.LINE
          }
        ]
      }
    }
    return (
      <Card
        inputs={inputs}
        onCompare={onCompare}
        onSuccess={onSuccess}
        url={'/assignments/lecture3/function1'}
        {...props}
        title={'Function as Line: y = x * 2'}
      />
    )
  }

  /**
   * Function as Scatter y = square root of the absolute value of ((x ^ 2) + (x * 4))
   */
  function2 = (props) => {
    const inputs = [
      [48, -50, 28, -27, -46, 4, 46, -49, 25, -25, -32, -16, 44, 20, -26, 11, 19, -48, 13, 22, 46, 8, 29, 5, -3, -27, 0, 11, 13, 36],
      [17, -12, -19, 37, -48, -26, -22, -39, 15, 15, -40, 34, -11, 6, -32, 43, 20, -46, -3, -18, 1, 23, -1, 45, 25, 14, 7, 6, 23, -24],
      [39, 4, 29, -38, 30, -14, -11, 28, -1, 41, 26, 15, -5, -1, 39, 35, -47, -2, 11, 28, -30, -30, 47, 39, -44, -44, -45, 38, -2, -36],
      [-14, 18, -36, 15, -33, -12, -40, -37, 29, -6, 28, 44, 15, 15, -18, -3, -45, -1, -23, -26, -6, 50, -33, -11, -6, -26, 47, 2, 12, 5],
      [8, 12, -38, 4, -15, 3, -23, 5, -22, 6, 23, -22, -11, -4, -49, -44, -6, -40, -45, 42, -25, -16, 16, 42, -45, 14, -37, -30, 46, -32],
      [],
      [14, 0, 0]
    ]
    const onCompare = (input, output) => {
      const result = input.map(x => Math.sqrt(Math.abs((Math.pow(x, 2)) + (x * 4))))
      return _.isEqual(result, output)
    }
    const onSuccess = (input, output) => {
      return {
        ...axisGraphDefaultOptions,
        series: [
          {
            data: input.map((next, index) => [input[index], output[index]]),
            type: GRAPH_TYPES.SCATTER
          }
        ]
      }
    }
    return (
      <Card
        inputs={inputs}
        onCompare={onCompare}
        onSuccess={onSuccess}
        url={'/assignments/lecture3/function2'}
        {...props}
        title={'Function as Scatter y = square root of the absolute value of ((x ^ 2) + (x * 4))'}
      />
    )
  }

  /**
   * Function y = If 3^2 - x^2 > 0 than square root of (3^2 - x^2). If 3^2 - x^2 < 0 then - square root of absolute value of (3^2 - x^2)
   */
  function3 = (props) => {
    const inputs = [
      [48, -50, 28, -27, -46, 4, 46, -49, 25, -25, -32, -16, 44, 20, -26, 11, 19, -48, 13, 22, 46, 8, 29, 5, -3, -27, 0, 11, 13, 36],
      [17, -12, -19, 37, -48, -26, -22, -39, 15, 15, -40, 34, -11, 6, -32, 43, 20, -46, -3, -18, 1, 23, -1, 45, 25, 14, 7, 6, 23, -24],
      [39, 4, 29, -38, 30, -14, -11, 28, -1, 41, 26, 15, -5, -1, 39, 35, -47, -2, 11, 28, -30, -30, 47, 39, -44, -44, -45, 38, -2, -36],
      [-14, 18, -36, 15, -33, -12, -40, -37, 29, -6, 28, 44, 15, 15, -18, -3, -45, -1, -23, -26, -6, 50, -33, -11, -6, -26, 47, 2, 12, 5],
      [8, 12, -38, 4, -15, 3, -23, 5, -22, 6, 23, -22, -11, -4, -49, -44, -6, -40, -45, 42, -25, -16, 16, 42, -45, 14, -37, -30, 46, -32],
      [],
      [14, 0, 0]
    ]
    const onCompare = (input, output) => {
      const result = input.map(x => {
        let y = 3**2 - x ** 2
        return y > 0 ? Math.sqrt(y) : -1 * Math.sqrt(Math.abs(y))
      })
      return _.isEqual(result, output)
    }
    const onSuccess = (response) => {
      return {
        ...axisGraphDefaultOptions,
        series: [
          {
            data: response,
            type: GRAPH_TYPES.SCATTER
          }
        ]
      }
    }
    const title = 'Function y = If 3^2 - x^2 > 0 than square root of (3^2 - x^2). If 3^2 - x^2 < 0 then -1 * square root of absolute value of (3^2 - x^2)'
    return (
      <Card
        inputs={inputs}
        onCompare={onCompare}
        onSuccess={onSuccess}
        url={'/assignments/lecture3/function3'}
        {...props}
        title={title}
      />
    )
  }

  /**
   * Function as Line: y = sin(x)
   */
  function4 = (props) => {
    const inputs = [
      randomValuesOfLength(300),
      [17, -12, -19, 37, -48, -26, -22, -39, 15, 15, -40, 34, -11, 6, -32, 43, 20, -46, -3, -18, 1, 23, -1, 45, 25, 14, 7, 6, 23, -24],
      [39, 4, 29, -38, 30, -14, -11, 28, -1, 41, 26, 15, -5, -1, 39, 35, -47, -2, 11, 28, -30, -30, 47, 39, -44, -44, -45, 38, -2, -36],
      [-14, 18, -36, 15, -33, -12, -40, -37, 29, -6, 28, 44, 15, 15, -18, -3, -45, -1, -23, -26, -6, 50, -33, -11, -6, -26, 47, 2, 12, 5],
      [8, 12, -38, 4, -15, 3, -23, 5, -22, 6, 23, -22, -11, -4, -49, -44, -6, -40, -45, 42, -25, -16, 16, 42, -45, 14, -37, -30, 46, -32],
      [],
      [14, 0, 0]
    ]
    const onCompare = (input, output) => {
      const result = input.map(x => Math.sin(x))
      if (output.length !== result.length) {
        return false
      }
      return result.every((x, index) => parseFloat(x).toFixed(2) === output[index].toFixed(2))
    }
    const onSuccess = (input, output) => {
      return {
        ...axisGraphDefaultOptions,
        series: [
          {
            data: input.map((next, index) => [input[index], output[index]]),
            type: GRAPH_TYPES.SCATTER
          }
        ]
      }
    }
    return (
      <Card
        inputs={inputs}
        onCompare={onCompare}
        onSuccess={onSuccess}
        url={'/assignments/lecture3/function4'}
        {...props}
        title={'Function as Line: y = sin(x)'}
      />
    )
  }

  /**
   * Function as Line: y = cos(x)
   */
  function5 = (props) => {
    const inputs = [
      randomValuesOfLength(300),
      [17, -12, -19, 37, -48, -26, -22, -39, 15, 15, -40, 34, -11, 6, -32, 43, 20, -46, -3, -18, 1, 23, -1, 45, 25, 14, 7, 6, 23, -24],
      [39, 4, 29, -38, 30, -14, -11, 28, -1, 41, 26, 15, -5, -1, 39, 35, -47, -2, 11, 28, -30, -30, 47, 39, -44, -44, -45, 38, -2, -36],
      [-14, 18, -36, 15, -33, -12, -40, -37, 29, -6, 28, 44, 15, 15, -18, -3, -45, -1, -23, -26, -6, 50, -33, -11, -6, -26, 47, 2, 12, 5],
      [8, 12, -38, 4, -15, 3, -23, 5, -22, 6, 23, -22, -11, -4, -49, -44, -6, -40, -45, 42, -25, -16, 16, 42, -45, 14, -37, -30, 46, -32],
      [],
      [14, 0, 0]
    ]
    const onCompare = (input, output) => {
      const result = input.map(x => Math.cos(x))
      if (output.length !== result.length) {
        return false
      }
      return result.every((x, index) => parseFloat(x).toFixed(2) === output[index].toFixed(2))
    }
    const onSuccess = (input, output) => {
      return {
        ...axisGraphDefaultOptions,
        series: [
          {
            data: input.map((next, index) => [input[index], output[index]]),
            type: GRAPH_TYPES.SCATTER
          }
        ]
      }
    }
    return (
      <Card
        inputs={inputs}
        onCompare={onCompare}
        onSuccess={onSuccess}
        url={'/assignments/lecture3/function5'}
        {...props}
        title={'Function as Line: y = cos(x)'}
      />
    )
  }

  /**
   *  2 Line Functions displayed together, one for Sin and one for Cos, check only the series option to include two of them
   * Check previous functions
   */
  function6 = (props) => {
    const inputs = [
      randomValuesOfLength(300).sort((a, b) => a - b),
      [17, -12, -19, 37, -48, -26, -22, -39, 15, 15, -40, 34, -11, 6, -32, 43, 20, -46, -3, -18, 1, 23, -1, 45, 25, 14, 7, 6, 23, -24],
      [39, 4, 29, -38, 30, -14, -11, 28, -1, 41, 26, 15, -5, -1, 39, 35, -47, -2, 11, 28, -30, -30, 47, 39, -44, -44, -45, 38, -2, -36],
      [-14, 18, -36, 15, -33, -12, -40, -37, 29, -6, 28, 44, 15, 15, -18, -3, -45, -1, -23, -26, -6, 50, -33, -11, -6, -26, 47, 2, 12, 5],
      [8, 12, -38, 4, -15, 3, -23, 5, -22, 6, 23, -22, -11, -4, -49, -44, -6, -40, -45, 42, -25, -16, 16, 42, -45, 14, -37, -30, 46, -32],
      [],
      [14, 0, 0]
    ]
    const onCompare = (input, output) => {
      if (output.length < 2) {
        return false
      }
      const sin = input.map(x => Math.sin(x))
      const cos = input.map(x => Math.cos(x))
      if (output[0].length !== sin.length) {
        return false
      }
      if (output[1].length !== cos.length) {
        return false
      }
      return sin.every((x, index) => parseFloat(x).toFixed(2) === output[0][index].toFixed(2)) &&
        cos.every((x, index) => parseFloat(x).toFixed(2) === output[1][index].toFixed(2))
    }
    const onSuccess = (input, output) => {
      return {
        ...axisGraphDefaultOptions,
        series: [
          {
            data: input.map((next, index) => [input[index], output[0][index]]),
            type: GRAPH_TYPES.LINE
          },
          {
            data: input.map((next, index) => [input[index], output[1][index]]),
            type: GRAPH_TYPES.LINE
          }
        ]
      }
    }
    const title = '2 Line Functions displayed together, one for Sin and one for Cos, check only the series option to include two of them'
    return (
      <Card
        inputs={inputs}
        onCompare={onCompare}
        onSuccess={onSuccess}
        url={'/assignments/lecture3/function6'}
        {...props}
        title={title}
      />
    )
  }

  /**
   * I want to see the top 4 performing words, given the randomCategoryData, the top 4 with the highest random generated value
   */
  function7 = (props) => {
    const inputs = Array(6).fill(null).map(() => randomCategoryData(36, true))
    const onCompare = (input, output) => {
      const result = input.reduce((accumulator, next, index) => {
        const isTheSame = (a, b) => a.name === b.name
        const found = accumulator.find(which => isTheSame(which, next))
        if (found) {
          return accumulator.map(which => {
            if (isTheSame(which, next)) {
              return {...which, value: which.value + next.value }
            }
            return which
          })
        }
        const { name, value } = next
        return accumulator.concat({ name, value })
      }, [])
      .sort((a, b) => b.value - a.value)
      .filter((which, index) => index < 4)
      return _.isEqual(result, output)
    }
    const onSuccess = (input, output) => {
      return {
        name: 'Tree',
        series: [
          {
            data: output,
            type: GRAPH_TYPES.TREEMAP
          }
        ]
      }
    }
    const title = 'Filter out top 4 most common words, based on their random generated value! Make sure the values are summed on repeated words (no duplicates)!'
    return (
      <Card
        inputs={inputs}
        onCompare={onCompare}
        onSuccess={onSuccess}
        url={'/assignments/lecture3/function7'}
        {...props}
        title={title}
      />
    )
  }

  /**
   * Calculate the average within the groups now, and show that here. Check the random Category data on how it generates those
   */
  function8 = (props) => {
    const inputs = Array(6).fill(null).map(() => randomCategoryData(36, true).map(next => ({ name: next.group, value: next.value })))
    const onCompare = (input, output) => {
      const groups = input.reduce((accumulator, next) => {
        const group = next.name
        return accumulator.includes(group) ? accumulator : [...accumulator, group]
      }, [])

      const grouped = groups.map(group => {
        const items = input.filter(next => next.name === group)
        const length = items.length || Infinity
        const average = items.reduce((sum, next) => sum + next.value, 0) / length
        return {
          name: group,
          value: average
        }
      })
      return grouped.every((item) => {
        return !!output.find(which => item.name === which.name && item.value === which.value)
      })
    }
    const onSuccess = (input, output) => {
      return {
        series: [
          {
            data: output,
            type: GRAPH_TYPES.PIE
          }
        ]
      }
    }
    return (
      <Card
        inputs={inputs}
        onCompare={onCompare}
        onSuccess={onSuccess}
        url={'/assignments/lecture3/function8'}
        {...props}
        title={'The Average within Groups'}
      />
    )
  }

  /**
   * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
   */
  function9 = (props) => {
    const inputs = Array(6).fill(null).map(() => randomCategoryData(36, true).map(next => ({ name: next.name, value: next.value })))
    const onCompare = (input, output) => {
      const cumulative = input.reduce((accumulator, next) => {
        const { sum, data } = accumulator
        const cumulatedValue = sum + next.value
        return { sum: cumulatedValue, data: [...data, {...next, value: cumulatedValue}]}
      }, { sum: 0, data: [] })
      return _.isEqual(cumulative.data, output)
    }
    const onSuccess = (input, output) => {
      return {
        xAxis: {
          type: 'category',
          data: output.map(next => next.name)
        },
        yAxis: {
          type: 'value',
        },
        series: [
          {
            data: output,
            type: GRAPH_TYPES.BAR
          }
        ]
      }
    }
    const title = 'Calculate the values such that they are cumulative, each subsequent is summed with the total so far!'
    return (
      <Card
        inputs={inputs}
        onCompare={onCompare}
        onSuccess={onSuccess}
        url={'/assignments/lecture3/function9'}
        {...props}
        title={title}
      />
    )
  }

  render() {
    const { classes, section } = this.props

    let graphFunctions = section.children[0]
    let binarySearch = section.children[1]

    const cardProps = {
      titleClass: classes.title,
      className: classes.card,
      graphClass: classes.graph
    }
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Home Assignments
          <Divider />
        </Typography>
        <Typography id={graphFunctions.id} variant={'title'}>
          {graphFunctions.display}
        </Typography>
        <Typography variant='p'>
          Title: "Implements and visualise Mathematical Functions:"<br/>
          Description: "Implement the graph functions according to definition"<br/>
          For more examples on available graphs take a look at: <SimpleLink href="https://ecomfe.github.io/echarts-examples/public/index.html">Echarts Demo</SimpleLink><br/>
          If you want to implement a new chart, then refer to the options: <SimpleLink href="https://ecomfe.github.io/echarts-doc/public/en/option.html">Echarts Graph Options</SimpleLink><br/>
        </Typography>
        <Typography fontStyle={'italic'} variant='p'>
          Tips and Tricks: You are going to use the "Math" function a lot in this assignment, check what the options are!
        </Typography>
        <div className={classes.graphs}>
          {this.function1(cardProps)}
          {this.function2(cardProps)}
          {this.function3(cardProps)}
          {this.function4(cardProps)}
          {this.function5(cardProps)}
          {this.function6(cardProps)}
          {this.function7(cardProps)}
          {this.function8(cardProps)}
          {this.function9(cardProps)}
        </div>
        <Typography id={binarySearch.id} variant={'title'}>
          {binarySearch.display}
        </Typography>
        <Typography variant='p'>
          Title: "Implement the Binary Search Function"<br/>
          Description: "Using Binary Search I will search for the given value at the given sorted array"<br/>
          To understand how binary search works visit: <SimpleLink href="https://www.tutorialspoint.com/data_structures_algorithms/binary_search_algorithm.htm">Binary Search Explenation</SimpleLink><br/>

        </Typography>
        <BinarySearch/>
      </Fragment>
    )
  }
}

export default withStyles(styles)(JavaAssignments)
