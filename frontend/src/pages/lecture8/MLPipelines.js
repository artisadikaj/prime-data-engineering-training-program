/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import {Bold, Italic} from "presentations/Label";
import ExploreSchema from "assets/images/lecture8/explore_schema.jpg";
import Code from "presentations/Code";


const styles = ({ typography }) => ({
  root: {},
})

const transformerCode = `import org.apache.spark.ml.feature.VectorAssembler
val vecAssembler = new VectorAssembler()
 .setInputCols(Array("bedrooms"))
 .setOutputCol("features")
val vecTrainDF = vecAssembler.transform(trainDF)
`
const regression = `import org.apache.spark.ml.regression.LinearRegression
val lr = new LinearRegression()
 .setFeaturesCol("features")
 .setLabelCol("price")
val lrModel = lr.fit(vecTrainDF)
`
const pipelineCode = `import org.apache.spark.ml.Pipeline
val pipeline = new Pipeline().setStages(Array(vecAssembler, lr))
val pipelineModel = pipeline.fit(trainDF)`

class MLPipelines extends React.Component {
  render() {
    const { classes, section } = this.props
    const ingestionExplore = section.children[0]
    const preparing = section.children[1]
    const usingEstimators = section.children[2]
    const creatingPipe = section.children[3]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          In this section, we will cover how to create and tune ML pipelines. The concept of
          pipelines is common across many ML frameworks as a way to organize a series of
          operations to apply to your data. In MLlib, the Pipeline API provides a high-level API
          built on top of DataFrames to organize your machine learning workflow. The Pipe‐line API is
          composed of a series of transformers and estimators, which we will discuss
          in-depth later.
        </Typography>

        <Typography>
          The intent of this chapter is not to show you every API in MLlib, but rather to equip
          you with the skills and knowledge to get started with using MLlib to build end-to-end
          pipelines. Before going into the details, let’s define some MLlib terminology:

          <ol>
            <li><Bold>Transformer</Bold>: Accepts a DataFrame as input, and returns a new DataFrame with one or more
              columns appended to it. Transformers do not learn any parameters from your
              data and simply apply rule-based transformations to either prepare data for
              model training or generate predictions using a trained MLlib model. They have
              a .transform() method.</li>
            <li><Bold>Estimator</Bold>: Learns (or “fits”) parameters from your DataFrame via a .fit() method and
              returns a Model, which is a transformer.</li>
            <li><Bold>Pipeline</Bold>: Organizes a series of transformers and estimators into a single model. While
              pipelines themselves are estimators, the output of pipeline.fit() returns a Pipe
              lineModel, a transformer.</li>
          </ol>
        </Typography>

        <Typography>
          While these concepts may seem rather abstract right now, the code snippets and
          examples in this chapter will help you understand how they all come together.
        </Typography>

        <Typography variant={'title'} id={ingestionExplore.id}>
          {ingestionExplore.display}
        </Typography>

        <Typography>
          Let’s take a quick peek at the data set and the corresponding schema
        </Typography>

        <Typography>
          <img src={ExploreSchema} />
        </Typography>

        <Typography>
          Our goal is to predict the price per night for a rental property, given our features.
        </Typography>

        <Typography>
          Before we begin feature engineering and modeling, we will divide our data set into
          two groups: train and test. Depending on the size of your data set, your train/test ratio
          may vary, but many data scientists use 80/20 as a standard train/test split. The model’s
          performance on the test set is a proxy for how well it will perform on unseen data
          (i.e., in the wild or in production), assuming that data follows similar distributions.
        </Typography>

        <Typography>
          We will keep 80% for the training set and set aside 20% of our
          data for the test set. Further, we will set a random seed for reproducibility, such that if
          we rerun this code we will get the same data points going to our train and test data
          sets, respectively. The value of the seed itself shouldn’t matter, but data scientists often
          like setting it to 42:

          <Code>val Array(trainDF, testDF) = airbnbDF.randomSplit(Array(.8, .2), seed=42)</Code>
        </Typography>

        <Typography>
          During your exploratory analysis, you should cache the training
          data set because you will be accessing it many times throughout the
          machine learning process.
        </Typography>

        <Typography variant={'title'} id={preparing.id}>
          {preparing.display}
        </Typography>

        <Typography>
          Now that we have split our data into training and test sets, let’s prepare the data to
          build a linear regression model predicting price given the number of bedrooms. Linear regression (like many
          other algorithms in Spark) requires that all the input features are contained within a single vector in your
          DataFrame. Thus, we need to transform our data.
        </Typography>

        <Typography>
          Transformers in Spark accept a DataFrame as input and return a new DataFrame
          with one or more columns appended to it. They do not learn from your data, but
          apply rule-based transformations using the transform() method.
        </Typography>

        <Typography>
          For the task of putting all of our features into a single vector, we will use the VectorAssembler transformer.
          VectorAssembler takes a list of input columns and creates a
          new DataFrame with an additional column, which we will call features. It combines
          the values of those input columns into a single vector:
        </Typography>

        <Typography>
          <Code>{transformerCode}</Code>
        </Typography>

        <Typography variant={'title'} id={usingEstimators.id}>
          {usingEstimators.display}
        </Typography>

        <Typography>
          After setting up our vectorAssembler, we have our data prepared and transformed
          into a format that our linear regression model expects. In Spark, LinearRegression is
          a type of estimator—it takes in a DataFrame and returns a Model. Estimators learn
          parameters from your data, have an estimator_name.fit() method, and are eagerly
          evaluated (i.e., kick off Spark jobs), whereas transformers are lazily evaluated.
        </Typography>

        <Typography>
          <Code>{regression}</Code>
        </Typography>

        <Typography>
          lr.fit() returns a LinearRegressionModel (lrModel), which is a transformer. In
          other words, the output of an estimator’s fit() method is a transformer. Once the
          estimator has learned the parameters, the transformer can apply these parameters to
          new data points to generate predictions.
        </Typography>

        <Typography variant={'title'} id={creatingPipe.id}>
          {creatingPipe.display}
        </Typography>

        <Typography>
          If we want to apply our model to our test set, then we need to prepare that data in the
          same way as the training set (i.e., pass it through the vector assembler). Oftentimes
          data preparation pipelines will have multiple steps, and it becomes cumbersome to
          remember not only which steps to apply, but also the ordering of the steps.
        </Typography>

        <Typography>
          This is the motivation for the Pipeline API: you simply specify the stages you want your data to
          pass through, in order, and Spark takes care of the processing for you. They provide
          the user with better code reusability and organization. In Spark, Pipelines are estimators, whereas
          PipelineModels—fitted Pipelines—are transformers. Let’s build our pipeline now:
        </Typography>

        <Typography>
          <Code>{pipelineCode}</Code>
        </Typography>

        <Typography>
          Another advantage of using the Pipeline API is that it determines which stages are
          estimators/transformers for you, so you don’t have to worry about specifying
          name.fit() versus name.transform() for each of the stages.
        </Typography>

        <Typography>
          Since pipelineModel is a transformer, it is straightforward to apply it to our test data
          set too: <Code>val predDF = pipelineModel.transform(testDF)</Code>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(MLPipelines)
