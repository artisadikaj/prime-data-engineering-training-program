/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import React from "react";
import Intro from "pages/lecture8/Intro";
import {PAGES} from "Constants";
import MLIntro from "pages/lecture8/MLIntro";
import MLPipelines from "pages/lecture8/MLPipelines";
import MLEvaluation from "pages/lecture8/MLEvaluation";
import MLExamples from "pages/lecture8/MLExamples";

const styles = ({ typography }) => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { classes, breadcrumbs, ...other } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section,
      ...other
    }


    switch (section.id) {
      case PAGES.LECTURE_8.ML_INTRO:
        return <MLIntro {...props} />
      case PAGES.LECTURE_8.ML_PIPELINES:
        return <MLPipelines {...props} />
      case PAGES.LECTURE_8.ML_EVALUATION:
        return <MLEvaluation {...props} />
      case PAGES.LECTURE_8.ML_EXAMPLES:
        return <MLExamples {...props} />
    }
    return <Intro {...props}/>
  }
}

export default withStyles(styles)(Index)
