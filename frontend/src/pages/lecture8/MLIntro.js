/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, {Fragment} from "react";
import {Bold} from "presentations/Label";
import Unsupervised from "assets/images/lecture8/ml_unsupervised_1.jpg";

const styles = ({typography}) => ({
  root: {},
})


class MLIntro extends React.Component {
  render() {
    const {classes, section} = this.props
    const whatIsML = section.children[0]
    const whySparkForML = section.children[1]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider/>
        </Typography>

        <Typography>
          Up until this point, we have focused on data engineering workloads with Apache
          Spark. Data engineering is often a precursory step to preparing your data for machine
          learning (ML) tasks, which will be the focus of this chapter.
        </Typography>

        <Typography>
          We live in an era in which
          machine learning and artificial intelligence applications are an integral part of our
          lives. Chances are that whether we realize it or not, every day we come into contact
          with ML models for purposes such as online shopping recommendations and advertisements,
          fraud detection, classification, image recognition, pattern matching, and
          more. These ML models drive important business decisions for many companies.
        </Typography>

        <Typography>
          According to this McKinsey study, 35% of what consumers purchase on Amazon and
          75% of what they watch on Netflix is driven by machine learning–based product recommendations.
          Building a model that performs well can make or break companies.
        </Typography>

        <Typography variant={'title'} id={whatIsML.id}>
          {whatIsML.display}
        </Typography>

        <Typography>
          Machine Learning is a process for extracting patterns from your data, using
          statistics, linear algebra, and numerical optimization.
        </Typography>

        <Typography>
          There are a few types of machine learning, including supervised, semi-supervised,
          unsupervised, and reinforcement learning.
        </Typography>

        <Typography>
          <ul>
            <li>
              <Bold>Supervised Learning</Bold>: your data consists of a set of input records, each of
              which has associated labels, and the goal is to predict the output label(s) given a new
              unlabeled input. These output labels can either be discrete or continuous, which
              brings us to the two types of supervised machine learning: classification and regression.
            </li>
            <li>
              <Bold>Unsupervised Learning</Bold>: obtaining the labeled data required by supervised machine learning can
              be very expensive and/or infeasible. This is where unsupervised machine learning comes into
              play. Instead of predicting a label, unsupervised ML helps you to better understand
              the structure of your data. As an example, consider the original unclustered data on the left.
              There is no known true label for each of these data points (x1 , x2), but by applying
              unsupervised machine learning to our data we can find the clusters that naturally form, as shown below:

              <img src={Unsupervised}/>
            </li>
          </ul>
        </Typography>

        <Typography variant={'title'} id={whySparkForML.id}>
          {whySparkForML.display}
        </Typography>

        <Typography>
          Spark is a unified analytics engine that provides an ecosystem for data ingestion, fea‐
          ture engineering, model training, and deployment. Without Spark, developers would
          need many disparate tools to accomplish this set of tasks, and might still struggle with
          scalability.
        </Typography>

        <Typography>
          Spark has two machine learning packages:
          <ol>
            <li><Bold>spark.mllib</Bold>: is the original machine learning API, based on the RDD API</li>
            <li><Bold>spark.ml</Bold>: the newer API based on DataFrames</li>
          </ol>
        </Typography>

        <Typography>
          With spark.ml, data scientists can use one ecosystem for their data preparation and
          model building, without the need to downsample their data to fit on a single
          machine. spark.ml focuses on O(n) scale-out, where the model scales linearly with
          the number of data points you have, so it can scale to massive amounts of data.
        </Typography>

      </Fragment>
    )
  }
}

export default withStyles(styles)(MLIntro)
