/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { connect } from 'react-redux';
import Code from "presentations/Code";
import DocumentationToDoImage from "assets/images/lecture11/documentation-todo.png";
import DocumentationDone from "assets/images/lecture11/documentation-done.png";
import ClassDocumentation from "assets/images/lecture11/class-documentation.png";
import CodingConventionMotivation from "assets/images/lecture11/coding_conventions_motivation.png";
import CodingWithStyle from "assets/images/lecture11/coding-with-style.png";
import GuardIfElse from "assets/images/lecture11/guard-if-else.png";
import WideHorizontalIfElse from "assets/images/lecture11/wide-horizontal-if-else.png";
import DragonBallWide from "assets/images/lecture11/dragon-ball-wide.png";
import JavaDocExample from "assets/images/lecture11/javadoc.png";
import { Bold, Italic } from "presentations/Label";
import SimpleLink from "presentations/rows/SimpleLink";

const styles = ({ typography }) => ({
  root: {},
})

const withoutDocumentation = `public double add (double a, double b) {
  return a + b;
}`

const withDocumentation = `/**
 * The add method, returns the sum of parameter a with b
 * @param a the first double
 * @param b the second double
 * @return a + b the sum of both
 */
public double add (double a, double b) {
  return a + b;
}`

const classDocumentation = `/**
 * A service class that handles serialization and deserialization between Java POJO, Bson Documents and JSON.
 * Created by Agon on 09/08/2020
 */
@Singleton
public class SerializationService {`

const aMoreDetailedExample = `/**
 * Fetches a user by id
 * @param id user Id
 * @return the requested user
 * @throws NoSuchElementException if the user was not found
 * @throws IllegalArgumentException if the id is not written correctly
 * @see User
 */
public User getById (String id) throws NoSuchElementException, IllegalArgumentException {
  ObjectId identifier = new ObjectId(id);
  User found = mongoDB
      .getMongoDatabase()
      .getCollection("users", User.class)
      .find(Filters.eq("_id", identifier))
      .first();
  if (found == null) {
    throw new NoSuchElementException("User not found");
  }
  return found;
}`

const sourceFileDocumentation = `package com.example.model;
/**
 * Implementation-free perspective to be read by developers 
 * who might not necessarily have the source code at hand
 * 
 * @author x,y,z 
 * @date 
 * @version 
 * @copyright
 * 
 */
import com.example.util.FileUtil;
/**
 * Optional class specific comment
 */
public class SomeClass {
  // Static variables in order of visibility
  public static final Integer PUBLIC_COUNT = 1;
  
  /**
	 * Prints hello to the console log
	 */
	public void sayHello () {
		Logger.of(this.getClass()).debug("Hello!");
	}
	...
}`

const ifChecksExamples = `// Avoid (x) Do not omit {}
if (condition) 
  statement;
// Avoid (x) 
if (x < 0) negative(x);
// Avoid (x)
if (a == b && c == d) {
...
}
// Prefer (✓️)
if ((a == b) && (c == d)) {
...
}
// Prefer (✓️)
if (condition) {
  statements;
} else if (condition) {
  statements;
} else if (condition) {
  statements;
}
// Avoid (x)
if ((condition1 && condition2) 
    || (condition3 && condition4)
    ||!(condition5 && condition6)) { //BAD WRAPS
    doSomethingAboutIt(); //MAKE THIS LINE EASY TO MISS
}
// Prefer (✓)
if ((condition1 && condition2) 
        || (condition3 && condition4)
        ||!(condition5 && condition6)) {
    doSomethingAboutIt();
}`

const ternaryOperators = `alpha = (aLongBooleanExpression) ? beta : gamma;
alpha = (aLongBooleanExpression) ? beta
        : gamma;
alpha = (aLongBooleanExpression)
        ? beta
        : gamma;`

const switchStatements = `switch (condition) {
  case ABC: 
    statements;
  /* falls through */
  case DEF:
    statements;
    break;
  default:
    statements;
     break;
}`

const exceptionsStyles = `// Avoid (x) - Not easy to read
throw new IllegalStateException("Failed to process request" + request.getId()
    + " for user " + user.getId() + " query: '" + query.getText()
    + "'");
// Prefer (✓) - Fairly easier to read
throw new IllegalStateException("Failed to process"
    + " request " + request.getId()
    + " for user " + user.getId()
    + " query: '" + query.getText() + "'");`


const optionBuilders = `// Avoid (x) - Not easy to read
List<User> items = mongoDB.getMongoDatabase()
				.getCollection("users", User.class).find().into(new ArrayList<>());;
// Prefer (✓) - Fairly easier to read
List<User> items = mongoDB
    .getMongoDatabase()
    .getCollection("users", User.class)
    .find()
    .into(new ArrayList<>());

// Prefer (✓️) - Fairly easier to read
Double chaining = integers
      .stream()
      .filter(next -> next >= 0)
      .map(next -> Math.pow(next, 2))
      .reduce(0.0, (total, next) -> total + next);`

const declarationAndAssignments = `// Prefer (✓️️) 
int level;           // indentation level
int sizeMeter;       // size of table
// Avoid (x) in favour of above
int level, sizeMeter;
// Prefer (✓️️) - Include unit in variable name or type
long pollIntervalMs; 
int fileSizeGb;
Amount<Integer, Data> fileSize;
// Avoid (x) mixing types
int foo, fooarray[]; 
// Avoid (x) - Do not separate with comma
Format.print(System.out, “error”), exit(1);
// Avoid (x) multiple assignment
fooBar.fChar = barFoo.lchar = 'c'; 
// Avoid (x) embedded assignments in attempt to increase performance // or save a line. I am guilty of doing this :(
d = (a = b + c) + r;
// Prefer (✓️️) over above 
a = b + c;
d = a + r;
// Prefer (✓️️) 
String[] args
// Avoid (x)
String args[]
// Prefer (✓️️) Long use "L" instead of "l" to avoid confusion with 1 
long timeout = 3000000000L;
// Avoid (x) - Hard to tell last letter is l and not 1 
long timeout = 3000000000l;`


const commentTypes = `/ Block comment
/*
 * Usage: Provides description of files, methods, data structures 
 * and algorithms. Can be used at the beginning of each file and 
 * before each method. Used for long comments that do not fit a 
 * single line. 1 Blank line to proceed after the block comment. 
 */
// Single line comment
if (condition) {
 /* Handle the condition. */
  ...
}
// Trailing comment
if (a == 2) {
 return TRUE;        /* special case */
} else {
 return isPrime(a);  /* works only for odd a */
}
// End of line comment 
if (foo > 1) {
  // Do a double-flip.
  ...
} else {
  return false; // Explain why here.
}
//if (bar > 1) {
//
// // Do a triple-flip.
// ...
//}
//else
// return false;`

const javaDocExample = `/**
 * Returns an Image object that can then be painted on the screen. 
 * The url argument must specify an absolute {@link URL}. The name
 * argument is a specifier that is relative to the url argument. 
 * <p>
 * This method always returns immediately, whether or not the 
 * image exists. When this applet attempts to draw the image on
 * the screen, the data will be loaded. The graphics primitives 
 * that draw the image will incrementally paint on the screen. 
 *
 * @param  url  an absolute URL giving the base location of the image
 * @param  name the location of the image, relative to the url argument
 * @return      the image at the specified URL
 * @see         Image
 */
 public Image getImage(URL url, String name) {
        try {
            return getImage(new URL(url, name));
        } catch (MalformedURLException e) {
            return null;
        }
 }`

const keyTags = `@author              => @author Raf
@code                => {@code A<B>C} 
@deprecated          => @deprecated deprecation-message 
@exception           => @exception IOException thrown when 
@link                => {@link package.class#member label} 
@param               => @param parameter-name description 
@return              => What the method returns
@see                 => @see "string" OR @see <a ...></a>
@since               => To indicate the version when a publicly accessible method is added`

const professionalExample = `// Avoid (x)
// I hate xml/soap so much, why can't it do this for me!?
try {
  userId = Integer.parseInt(xml.getField("id"));
} catch (NumberFormatException e) {
  ...
}
// Prefer (✓️)
// TODO(Jim): Tuck field validation away in a library.
try {
  userId = Integer.parseInt(xml.getField("id"));
} catch (NumberFormatException e) {
  ...
}`

const toDoTags = `// TODO: Implement this function
return CompletableFuture.supplyAsync(() -> {
  // TODO: do something smarter here...
  return new ArrayList<>();
}, ec.current());`

const guardImplementations = `
	public User getById (String id) throws NoSuchElementException, IllegalArgumentException {
		if (Strings.isNullOrEmpty(id)) {
			throw new IllegalArgumentException("Missing ID");
		}

		... code
  }
		
	public User getById (String id) throws NoSuchElementException, IllegalArgumentException {
		if (!Strings.isNullOrEmpty(id)) {
			...code	
		}
		throw new IllegalArgumentException("Missing ID");
  }`

const buildSbtSwagger = `libraryDependencies ++= Seq(
  ...
  // swagger
  "io.swagger" %% "swagger-play2" % "1.7.1"
)`

const swaggerRoutes = `GET           /swagger.json                                                      controllers.ApiHelpController.getResources
GET           /docs/*file                                                        controllers.Assets.at(path="/public/swagger",file)`
class Documentation extends React.Component {

  render() {
    const { classes, section } = this.props
    const rest = section.children[0]
    const documentation = section.children[1]
    const codingStyles = section.children[2]
    const swaggerDocumentation = section.children[3]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography id={rest.id} variant={'title'}>
          {rest.display}
        </Typography>
        <Typography>
          Tools like:
          <ol>
            <li>Postman: <SimpleLink href="https://www.postman.com/api-documentation-tool/">https://www.postman.com/api-documentation-tool/</SimpleLink></li>
            <li>Swagger: <SimpleLink href="https://swagger.io/tools/swagger-ui/">https://swagger.io/tools/swagger-ui/</SimpleLink></li>
            <li>and others...</li>
          </ol>
          Can be used to generate a documentation page/sheet/paper of your API routes that are exposed from your backend API! On this section we have been using PostMan to document the work behind our API!
        </Typography>
        <Typography id={documentation.id} variant={'title'}>
          {documentation.display}
        </Typography>
        <Typography>
          In general it is a good idea to document functions and classes and everything really, in order to help other developers or sometimes also yourself, in cases where you have to get back to a piece of code that you wrote a month ago as an example, to better read or understand the purpose and/or behaviour of the code.
        </Typography>
        <Typography>
          Most of the IDE-s will react based on your documentation and provide meaningful assistance when you are trying to invoke a function, assign a property etc. Let us look at the following example:
          <Code>
            {withoutDocumentation}
          </Code>
          <img src={DocumentationToDoImage} />
        </Typography>
        <Typography>
          By default WebStorm or Visual Studio Code, helps you out by pointing the parameters that this method accepts and the return value.
        </Typography>
        <Typography>
          Let us see what happens if we apply some documentation to it:
          <Code>
            {withDocumentation}
          </Code>
          <img src={DocumentationDone} />
        </Typography>
        <Typography>
          Classes can also be documented like this:
          <Code>
            {classDocumentation}
          </Code>
          And here is an example call:
        </Typography>
        <img src={ClassDocumentation} />

        <Typography>
          Here is a more detailed example with exceptions documented:
          <Code>
            {aMoreDetailedExample}
          </Code>
        </Typography>
        <Typography>
          Here are some examples of different types of comments:
          <Code>
            {commentTypes}
          </Code>
        </Typography>
        <Typography>
          The general idea behind this is to explain and clarify confusion and maximise productivity! Be the good guy/girl and share what you have written. Remember <Bold>sharing means caring</Bold>!
        </Typography>
        <img src={CodingConventionMotivation} />

        <Typography>
          A bonus point with having good documentation is that you can actually generate Documentation Pages using tools like Java Doc
          <ul>
            <li>Javadoc - <SimpleLink href="https://en.wikipedia.org/wiki/Javadoc">https://en.wikipedia.org/wiki/Javadoc</SimpleLink></li>
            <li>How to write doc documents for the java doc - <SimpleLink href="https://www.oracle.com/technical-resources/articles/java/javadoc-tool.html">https://www.oracle.com/technical-resources/articles/java/javadoc-tool.html</SimpleLink></li>
          </ul>
          Here is an example:
          <Code>
            {javaDocExample}
          </Code>
          Which will result in the following generated HTML if you run java doc against code that has the above
        </Typography>
        <img src={JavaDocExample} />
        <Typography>
          A different example of the generated HTML: <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/mongodb-driver-sync/com/mongodb/client/package-tree.html">https://mongodb.github.io/mongo-java-driver/4.1/apidocs/mongodb-driver-sync/com/mongodb/client/package-tree.html</SimpleLink>
        </Typography>
        <Typography>
          Here are some key tags that you can use to enhance the quality of the generated java documentation.
          <Code>
            {keyTags}
          </Code>
          And of course be professional:
          <Code>
            {professionalExample}
          </Code>
          If something is not ready add the TODO tag:
          <Code>
            {toDoTags}
          </Code>
        </Typography>
        <Typography id={codingStyles.id} variant={'title'}>
          {codingStyles.display}
        </Typography>
        <Typography variant="section">
          Naming Conventions
        </Typography>
        <Typography>
          Class and interface names are CamelCase and it is recommended to use the whole word and avoid acronyms/abbreviations. For example <Italic>class Raster</Italic> or <Italic>class ImageSprite</Italic>
        </Typography>
        <Typography>
          Java naming conventions are set of rules to make Java code look uniform across Java projects and the library. They are not strict rules, but a guideline to adhere to as a good programming practice. It is, therefore, not a good idea to violate the sanctity of the code uniformity either due to haste or rebellion. The rules are pretty simple.
          <ul>
            <li>Package names are types in lowercase: <Italic>javax.sql, org.junit, java.lang.</Italic></li>
            <li>Class, enum, interface, and annotation names are typed in uppercase: <Italic>Thread, Runnable, @Override.</Italic></li>
            <li>Methods and field names are typed in a camel case: <Italic>deleteCharAt, add, isNull.</Italic></li>
            <li>Constants are types in uppercase separated by an underscore: <Italic>SIZE, MIN_VALUE.</Italic></li>
            <li>Local variables are typed in camel case: <Italic>employeeName, birthDate, email.</Italic></li>
          </ul>
          These conventions have become so much apart of Java that almost every programmer follows them religiously. As a result, any change in this looks outlandish and wrong. An obvious benefit of following these conventions is that the style of code aligns with the library or framework code. It also helps other programmers to quickly pick up the code when they have to, therefore leveraging overall readability of the code.
        </Typography>
        <Typography>
          Formatting and indentation are all about organizing your code to make it easy to read, and it includes spacing, line length, wraps and breaks and so on
        </Typography>
        <Typography>
          <ul>
            <li><Bold>Indentation</Bold> — Use 2 or 4 spaces or tabs and stay consistent</li>
            <li><Bold>Line length</Bold> — Up to 70 to 120 characters depending on the effect on readability. It’s important to eliminate the need for horizontal scrolling and place line breaks after a comma and operator.</li>
          </ul>
          Here are some examples of if checks:
          <Code>
            {ifChecksExamples}
          </Code>
        </Typography>

        <Typography variant="section">
          Java Source File
        </Typography>
        <Typography>
          The following is considered as best practices when it comes to java source files:
          <ul>
            <li>The source file length is lower than 2,000 lines of code</li>
            <li>The source file is organized with documentation comment, package declaration, followed by a class comment, imports grouped (static last), class/interface signature and so on as shown below</li>
          </ul>
          <Code>
            {sourceFileDocumentation}
          </Code>
          Ternary operations:
          <Code>
            {ternaryOperators}
          </Code>
          When it comes to switch it’s best practice to:
          <ol>
            <li>Always have a default case even without code</li>
            <li>Use /* falls through */ to indicate the control falls to the next case</li>
          </ol>
          <Code>
            {switchStatements}
          </Code>
          When throwing an exception here are samples of good and poorly indented messages.
          <Code>
            {exceptionsStyles}
          </Code>
          Option builders (Streams, Mongo connection etc)
          <Code>
            {optionBuilders}
          </Code>
          Declarations and assignments:
          <Code>
            {declarationAndAssignments}
          </Code>
          Method invocation guards:
          <Code>
            {guardImplementations}
          </Code>
          Avoid doing:
        </Typography>
        <img src={WideHorizontalIfElse} />
        <img src={DragonBallWide} />
        <Typography>
          Instead aim for:
        </Typography>
        <img src={GuardIfElse} />
        <Typography>
          For more interactive examples visit <SimpleLink href="https://medium.com/better-programming/refactoring-guard-clauses-2ceeaa1a9da">https://medium.com/better-programming/refactoring-guard-clauses-2ceeaa1a9da</SimpleLink>
        </Typography>
        <img src={CodingWithStyle} />

        <Typography id={swaggerDocumentation.id} variant={'title'}>
          {swaggerDocumentation.display}
        </Typography>
        <Typography>
          In order to get the API Documentation setup with swagger we need first to:<br/>
          Get the swagger library at build.sbt:
          <Code>
            {buildSbtSwagger}
          </Code>
          At application.conf:
          <Code>
            {`play.modules.enabled += "play.modules.swagger.SwaggerModule"`}
          </Code>
          At routes:
          <Code>
            {swaggerRoutes}
          </Code>
          Our documentation renderer can be found:
          <Code>
            {`GET           /docs                                                              @io.training.api.controllers.ApplicationController.swagger(request: Request)`}
          </Code>
        </Typography>
        <Typography>
          For more information visit:
          <ol>
            <li><SimpleLink href="https://medium.com/@rhamedy/a-short-summary-of-java-coding-best-practices-31283d0167d3">https://medium.com/@rhamedy/a-short-summary-of-java-coding-best-practices-31283d0167d3</SimpleLink></li>
            <li><SimpleLink href="https://www.developer.com/java/data/top-10-java-coding-guidelines.html">https://www.developer.com/java/data/top-10-java-coding-guidelines.html</SimpleLink></li>
            <li>Oracle: <SimpleLink href="https://www.oracle.com/java/technologies/javase/codeconventions-contents.html">https://www.oracle.com/java/technologies/javase/codeconventions-contents.html</SimpleLink></li>
            <li>Oracle: <SimpleLink href="https://www.oracle.com/technetwork/java/codeconventions-150003.pdf">https://www.oracle.com/technetwork/java/codeconventions-150003.pdf</SimpleLink></li>
            <li>Google: <SimpleLink href="https://google.github.io/styleguide/javaguide.html">https://google.github.io/styleguide/javaguide.html</SimpleLink></li>
            <li>Twitter: <SimpleLink href="https://github.com/twitter-archive/commons/blob/master/src/java/com/twitter/common/styleguide.md">https://github.com/twitter-archive/commons/blob/master/src/java/com/twitter/common/styleguide.md</SimpleLink></li>
            <li>Spring: <SimpleLink href="https://github.com/spring-projects/spring-framework/wiki/Code-Style">https://github.com/spring-projects/spring-framework/wiki/Code-Style</SimpleLink></li>
          </ol>
        </Typography>
      </Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = (dispatch) => ({
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Documentation))
