/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import React from "react";
import { PAGES } from 'Constants'
import Intro from "pages/lecture9/Intro";
import Documentation from "pages/lecture9/Documentation";
import CDCI from "pages/lecture9/CDCI";
import DataValidation from "pages/lecture9/DataValidation";

const styles = ({ typography }) => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { classes, breadcrumbs, ...other } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section,
      ...other
    }

    switch (section.id) {
      case PAGES.LECTURE_9.DOCUMENTATION:
        return <Documentation {...props} />
      case PAGES.LECTURE_9.CD_CI:
        return <CDCI {...props} />
      case PAGES.LECTURE_9.DATA_VALIDATION:
        return <DataValidation {...props} />
    }
    return <Intro {...props}/>
  }
}

export default withStyles(styles)(Index)
