import { PAGES } from 'Constants'

const routes = [
  {
    display: 'Home',
    id: PAGES.HOME,
  },
  {
    display: '1. Setup and Introduction',
    id: PAGES.LECTURE_1.ID,
    children: [
      {
        display: 'Getting Started',
        id: PAGES.LECTURE_1.GETTING_STARTED,
        children: [
          {
            display: 'GitLab',
          },
          {
            display: 'Source Tree'
          },
          {
            display: 'SBT (Scala Build Tool)'
          },
          {
            display: 'IntelliJ or VS Code'
          },
          {
            display: 'Slack'
          },
          {
            display: 'MongoDB'
          },
          {
            display: 'NPM'
          },
          {
            display: 'Postman'
          },
          {
            display: 'Redis'
          }
        ]
      },
      {
        display: 'Project Setup',
        id: PAGES.LECTURE_1.PROJECT_SETTUP,
        children: [
          {
            display: 'Seting up the repository'
          },
          {
            display: 'Running the Application'
          }
        ]
      },
      {
        display: 'Agile Methodology',
        id: PAGES.LECTURE_1.AGILE_METHODOLOGY,
        children: [
          {
            display: 'Scrum'
          }
        ]
      },
      {
        display: 'Working with GIT (Source Tree or Terminal)',
        id: PAGES.LECTURE_1.WORKING_WITH_GIT,
        children: [
          {
            display: 'Workflow'
          },
          {
            display: 'Add & Commit'
          },
          {
            display: 'Pushing Changes'
          },
          {
            display: 'Branching'
          },
          {
            display: 'Update and Merge'
          }
        ]
      },
      {
        display: 'Way of Working',
        id: PAGES.LECTURE_1.WAY_OF_WORKING,
        children: [
          {
            display: 'GitLab Setup',
          },
          {
            display: 'Rules',
          },
          {
            id: 'way_of_working_exercise_1',
            display: 'Exercise 1'
          }
        ]
      }
    ]
  },
  {
    display: '2. Big Data Ecosystem',
    id: PAGES.LECTURE_2.ID,
    children: [
      {
        display: 'Introduction',
        id: PAGES.LECTURE_2.INTRODUCTION,
        children: [
          {
            display: 'What is Big Data?'
          },
          {
            display: 'What Does a Big Data Life Cycle Look Like?'
          },
          {
            display: 'Ingesting Data into the System'
          },
          {
            display: 'Persisting the Data in Storage'
          },
          {
            display: 'Computing and Analyzing Data'
          },
          {
            display: 'Visualizing the Results'
          },
          {
            display: 'Examples Of Big Data'
          }
        ]
      },
      {
        display: 'Hadoop',
        id: PAGES.LECTURE_2.HADOOP,
        children: [
          {
            display: 'Architecture'
          },
          {
            display: 'Storage Mechanisms'
          }
        ]
      },
      {
        display: 'Hive',
        id: PAGES.LECTURE_2.HIVE,
        children: [
          {
            id: 'hive_architecture',
            display: 'Architecture'
          },
          {
            display: 'Hive QL'
          }
        ]
      }
    ]
  },
  {
    display: '3. Spark',
    id: PAGES.LECTURE_3.ID,
    children: [
      {
        display: 'Introduction',
        id: PAGES.LECTURE_3.INTRODUCTION,
        children: [
          {
            display: 'Components'
          },
          {
            display: 'Spark Cluster Architecture'
          },
          {
            display: 'Cluster Types'
          }
        ]
      },
      {
        display: 'Spark RDD API',
        id: PAGES.LECTURE_3.SPARK_RDD,
        children: [
          { display: 'Apache Spark with Java' },
          { display: 'Resilient Distributed Datasets (RDDs)' },
          { display: 'RDD Operations' }
        ]
      },
      {
        display: 'Spark Dataset API',
        id: PAGES.LECTURE_3.SPARK_DATASET,
        children: [
          { display: 'SQL' },
          { display: 'Datasets and DataFrames' },
          { display: 'SparkSession' },
          { display: 'Creating DataFrames' },
          { display: 'Row' },
          { display: 'Dataset Operations' }
        ]
      },
      {
        display: 'Spark SQL',
        id: PAGES.LECTURE_3.SPARK_SQL,
        children: [
          { display: 'Spark and Apache Hive example' }
        ]
      },
      {
        display: 'Spark Java RDD Assignments',
        id: PAGES.LECTURE_3.SPARK_JAVA_ASSIGNMENTS,
        children: [
          { display: 'Graph Functions' },
          { display: 'Binary Search Sort' }
        ]
      },
      {
        display: 'Spark Data Frame Assignments',
        id: PAGES.LECTURE_3.SPARK_DATA_ASSIGNMENTS,
        children: [
        ]
      }
    ]
  },
  {
    display: '4. The Mighty SQL',
    id: PAGES.LECTURE_4.ID,
    children: [
      {
        display: 'SQL Select Statement',
        id: PAGES.LECTURE_4.SELECT,
        children: []
      },
      {
        display: 'SQL Aggregate Functions',
        id: PAGES.LECTURE_4.AGGREGATE,
        children: []
      },
      {
        display: 'SQL Join statements',
        id: PAGES.LECTURE_4.JOIN,
        children: [
          {
            display: 'SQL VIEW Statement'
          }
        ]
      },
      {
        display: 'Planning and Design',
        id: PAGES.LECTURE_4.DESIGN,
        children: [
          {
            display: 'Database designing'
          },
          {
            display: 'Normalization'
          }
        ]
      },
      {
        display: 'Data Types',
        id: PAGES.LECTURE_4.DATA_TYPES,
        children: [
          {
            display: 'SQL Create Constraints'
          }
        ]
      },
      {
        display: 'Assignments',
        id: PAGES.LECTURE_4.ASSIGNMENTS,
        children: [
          { display: 'SQL Expressions' },
          { display: 'Knight Tour' },
        ]
      }
    ]
  },
  {
    display: '5. AWS & Cloud Computing',
    id: PAGES.LECTURE_5.ID,
    children: [
      {
        display: 'Introduction to Cloud Computing',
        id: PAGES.LECTURE_5.CLOUD_COMPUTING_INTRO,
        children: [ ]
      },
      {
        display: 'AWS Cloud Provider',
        id: PAGES.LECTURE_5.AWS_CLOUD_PROVIDER,
        children: [
          {
            display: 'AWS Global Infrastructure Map'
          },
        ]
      },
      {
        display: 'What is Amazon EC2?',
        id: PAGES.LECTURE_5.AWS_EC2,
        children: [
          {
            display: 'Features of Amazon EC2'
          },
          {
            display: 'Instances and AMI'
          },
          {
            display: 'Instance Types'
          },
          {
            display: 'Instance Purchasing Options'
          },
          {
            display: 'Storage'
          },
          {
            display: 'EC2 Security Group'
          },
          {
            display: 'VPC (Virtual Private Cloud)'
          },
        ]
      },
    ]
  },
  {
    display: '6. AWS Big Data Services',
    id: PAGES.LECTURE_6.ID,
    children: [
      {
        display: 'Introduction to AWS Big Data Services',
        id: PAGES.LECTURE_6.AWS_BIG_DATA_SERVICES_INTRO,
        children: [
          {
            display: 'What is Serverless?'
          },
          {
            display: 'Why use Serverless?'
          },
        ]
      },
      {
        display: 'AWS S3',
        id: PAGES.LECTURE_6.AWS_S3,
        children: [
          {
            display: 'What is AWS S3?'
          },
          {
            display: 'AWS S3 - Versioning'
          },
          {
            display: 'S3 Encryption for Objects'
          },
          {
            display: 'Encryption in transit (SSL)'
          },
          {
            display: 'S3 Storage Tiers'
          },
          {
            display: 'S3 Standard – General Purpose'
          },
          {
            display: 'S3 Lifecycle Rules'
          },
        ]
      },
      {
        display: 'AWS Glue',
        id: PAGES.LECTURE_6.AWS_GLUE,
        children: [
          {
            display: 'Ideal Usage Patterns',
            id: 'glue-ideal-usage-pattern'
          },
          {
            display: 'Performance',
            id: 'glue-performance'
          },
          {
            display: 'Scalability and Elasticity',
            id: 'glue-scalability-and-elasticity'
          },
          {
            display: 'Reference Architecture',
            id: 'glue-reference-architecture'
          },
        ]
      },
      {
        display: 'AWS Athena',
        id: PAGES.LECTURE_6.AWS_ATHENA,
        children: [
          {
            display: 'Ideal Usage Patterns',
            id: 'athena-ideal-usage-patterns'
          },
          {
            display: 'Performance',
            id: 'athena-performance'
          },
          {
            display: 'Scalability and Elasticity',
            id: 'athena-scalability-and-elasticity'
          },
          {
            display: 'Reference Architecture',
            id: 'athena-reference-architecture'
          },
        ]
      },
    ]
  },
  {
    display: '7. AWS EMR',
    id: PAGES.LECTURE_7.ID,
    children: [
      {
        display: 'Introduction to AWS EMR',
        id: PAGES.LECTURE_7.EMR_INTRO,
        children: [
          {
            display: 'Benefits'
          },
          {
            display: 'Understanding an EMR Cluster'
          },
          {
            display: 'Submitting Work to a Cluster'
          },
          {
            display: 'Processing Data'
          },
          {
            display: 'Understanding the Cluster Lifecycle'
          },
        ]
      },
      {
        display: 'EMR Architecture',
        id: PAGES.LECTURE_7.EMR_ARCHITECTURE,
        children: [
          {
            display: 'Storage'
          },
          {
            display: 'Cluster Resource Management'
          },
          {
            display: 'Data Processing Frameworks'
          },
          {
            display: 'Applications and Programs'
          }

        ]
      },
      {
        display: 'Other Features',
        id: PAGES.LECTURE_7.EMR_OTHER_FEATURES,
        children: [
          {
            display: 'AWS Integration'
          },
          {
            display: 'Scalability and Flexibility'
          },
          {
            display: 'Reliability'
          },
          {
            display: 'Security'
          },
          {
            display: 'Monitoring'
          },
          {
            display: 'Management Interfaces'
          }
        ]
      },
    ]
  },
  {
    display: '8. Spark ML',
    id: PAGES.LECTURE_8.ID,
    children: [
      {
        id: PAGES.LECTURE_8.ML_INTRO,
        display: "Introduction to Spark ML",
        children: [
          {
            id: "what-is-machine-learning",
            display: "What is Machine Learning?"
          },
          {
            id: "why-spark-for-ML",
            display: "Why Spark for Machine Learning?"
          }
        ]
      },
      {
        id: PAGES.LECTURE_8.ML_PIPELINES,
        display: "Designing ML Pipelines",
        children: [
          {
            id: "data-ingestion-and-exploration",
            display: "Data Ingestion and Exploration"
          },
          {
            id: "preparing-features-with-transformers",
            display: "Preparing Features with Transformers"
          },
          {
            id: "using-estimators-to-build-models",
            display: "Using Estimators to Build Models"
          },
          {
            id: "creating-a-pipeline",
            display: "Creating a Pipeline"
          }
        ]
      },
      {
        id: PAGES.LECTURE_8.ML_EVALUATION,
        display: "Evaluating Models",
        children: [
          {
            id: "rmse",
            display: "RMSE"
          },
          {
            id: "r-squared",
            display: "R^2"
          },
          {
            id: "saving-and-loading-models",
            display: "Saving and Loading Models"
          }
        ]
      },
      {
        id: PAGES.LECTURE_8.ML_EXAMPLES,
        display: "Spark ML Examples"
      }
    ]
  },
  {
    display: '9. Documentation, Validations and CD/CI',
    id: PAGES.LECTURE_9.ID,
    children: [
      {
        display: 'Documentation and Coding Styles',
        id: PAGES.LECTURE_9.DOCUMENTATION,
        children: [
          { display: 'Rest Documentation Tools' },
          { display: 'Java Documentation' },
          { display: 'Coding Conventions'},
          { display: 'API Documentation with Swagger'},
        ]
      },
      {
        display: 'Continuous Development/Integration',
        id: PAGES.LECTURE_9.CD_CI,
        children: [
          { display: 'Introduction', id: "cd_ci_introduction" },
          { display: 'Continuous Integration' },
          { display: 'Continuous Delivery'},
          { display: 'Continuous Deployment'},
          { display: 'Workflow', id: 'cd_ci_workflow' },
          { display: 'Setup', id: 'cd_ci_setup' }
        ]
      },
      {
        display: "Data Validation",
        id: PAGES.LECTURE_9.DATA_VALIDATION,
        children: [
          {
            id: 'why-data-validating',
            display: 'Why Validate?'
          },
          {
            id: 'validating-rules-for-consistency',
            display: 'Validation Rules for Consistency'
          },
          {
            id: 'format-standards',
            display: 'Format Standards'
          },
          {
            id: 'schema-validation',
            display: 'Schema Validation'
          },
          {
            id: 'content-validation',
            display: 'Content Validation'
          }
        ]
      }
    ],
  },
  {
    display: 'Materials',
    id: PAGES.SUPPORT.ID,
    children: [
      {
        display: 'Glossary',
        id: PAGES.SUPPORT.GLOSSARY,
      },
      {
        display: 'Resources',
        id: PAGES.SUPPORT.RESOURCES,
      },
    ]
  }
]

const format = (which) => {
  let children = which.children || []
  return {
    ...which,
    id: !which.id ? which.display.replace(/ /g, '').replace(/[^a-zA-Z]/g, '').toLowerCase() : which.id,
    children: children.map(format)
  }
}

export const findById = (id) => {
  return routes.reduce((flatten, next) => {
    let children = next.children || []
    return [...flatten, next, ...children]
  }, []).find(which => which.id === id)
}

/**
 * Created by LeutrimNeziri on 30/03/2019.
 */
export default routes.map(format)


