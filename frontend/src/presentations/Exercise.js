/**
 * Created by LeutrimNeziri on 28/02/2019.
 */
import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import withGetRequestTests from 'middleware/withGetRequestTests'
import Code from 'presentations/Code'
import LoadingIndicator from 'presentations/LoadingIndicator'
import ErrorBox from 'presentations/ErrorBox'

const styles = ({palette, size, typography}) => ({
  root: {
    width: '100%',
    height: 320,
    overflowY: 'auto',
    position: 'relative'
  }
})
const Exercise = (props) => {
  const {classes, url} = props
  const {response, isLoading, error, runTests} = withGetRequestTests(url)
  return (
    <div className={classes.root}>
      <LoadingIndicator show={isLoading} isInside={true}/>
      {!!error && <ErrorBox message={error.message} onRetryClicked={runTests}/>}
      {!!response && <Code>
        {JSON.stringify(response, null, " ")}
      </Code>}
    </div>
  )
}

export default withStyles(styles)(Exercise)
