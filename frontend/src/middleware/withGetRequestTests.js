import React from "react";
import withTests from 'middleware/withTests'

/**
 * For the given inputs, has a comparison function and a success function
 * Runs the whole tests at the backend, and then compares each of them via the comparator
 * If all are success returns a good status code
 * @param url
 * @param onCompare
 * @param onSuccess
 * @param onFailure
 * @returns {{response: any, error: , isLoading: boolean, runTests: runTests}}
 */
function withGetRequestTests (url, onCompare, onSuccess, onFailure) {
  return withTests(
    ['main'],
    (input) => ({
      endpoint: url
    }),
    onCompare,
    onSuccess,
    onFailure
  )
}

export default withGetRequestTests
