import React from "react";
import { CALL_API } from 'middleware/Api';
import { useDispatch } from "react-redux"

/**
 * For the given inputs, has a comparison function and a success function
 * Runs the whole tests at the backend, and then compares each of them via the comparator
 * If all are success returns a good status code
 * @param inputs
 * @param options
 * @param onCompare
 * @param onSuccess
 * @param onError
 * @param enabled
 * @returns {{response: any, error: , isLoading: boolean, runTests: runTests}}
 */
function withTests (inputs, options = (input) => {}, onCompare = () => true, onSuccess = (input, output) => output, onError = () => {}, enabled = true) {
  const [response, setResponse] = React.useState(null)
  const [error, setError] = React.useState()
  const [isLoading, setIsLoading] = React.useState(true)
  const dispatch = useDispatch()


  const runTests = () => {
    if (inputs.length === 0) {
      return setIsLoading(false)
    }
    setIsLoading(true)
    setError(null)
    const promises = inputs.map(input => {
      return dispatch({
        [CALL_API]: options(input)
      }).then((response) => {
        if (!onCompare(input, response)) {
          throw { status: 400, message: 'Your implementation is wrong, check the code and re-run the tests!'}
        }
        return {
          input,
          output: response
        }
      })
    })
    return Promise.all(promises).then((results) => {
      console.log('all finished', results)
      const { input, output } = results[0]
      setResponse(onSuccess(input, output))
      setIsLoading(false)
      setError(null)
    }, (error) => {
      console.log('error', error)
      setResponse(onError(inputs, error))
      setError(error)
      setIsLoading(false)
    })
  }
  React.useEffect(() => {
    // start loading the data
    if (!!enabled) {
      runTests()
    }
  }, [enabled])

  return {
    response,
    error,
    isLoading,
    runTests
  }
}

export default withTests
