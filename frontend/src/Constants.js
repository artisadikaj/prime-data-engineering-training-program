/**
 * Created by LeutrimNeziri on 30/03/2019.
 */
module.exports = {
  API_URL: 'http://localhost:9000/api',
  PAGES: {
    HOME: 'home',
    LECTURE_1: {
      ID: 'lecture1',
      GETTING_STARTED: 'gettingStarted',
      PROJECT_SETTUP: 'projectSetup',
      AGILE_METHODOLOGY: 'agile',
      WAY_OF_WORKING: 'wayofWorking',
      WORKING_WITH_GIT: 'git'
    },
    LECTURE_2: {
      ID: 'lecture2',
      INTRODUCTION: 'bigdata',
      HADOOP: 'hadoop',
      HIVE: 'hive',
    },
    LECTURE_3: {
      ID: 'lecture3',
      INTRODUCTION: 'spark',
      SPARK_RDD: 'spark_rdd',
      SPARK_DATASET: 'spark_dataset',
      SPARK_SQL: 'spark_sql',
      SPARK_JAVA_ASSIGNMENTS: 'spark_java_assignments',
      SPARK_DATA_ASSIGNMENTS: 'spark_dataset_assignments',
    },
    LECTURE_4: {
      ID: 'lecture4',
      SELECT: 'select_statements',
      AGGREGATE: 'aggregate_statements',
      JOIN: 'join_statements',
      DESIGN: 'planing_design',
      DATA_TYPES: 'data_types',
      ASSIGNMENTS: 'lecture_four_assignments'
    },
    LECTURE_5: {
      ID: 'lecture5',
      CLOUD_COMPUTING_INTRO: 'introduction_to_cloud_computing',
      AWS_CLOUD_PROVIDER: 'aws_cloud_provider',
      AWS_EC2: "amazon_ec2"
    },
    LECTURE_6: {
      ID: 'lecture6',
      AWS_BIG_DATA_SERVICES_INTRO: 'aws_big_data_services_introduction',
      AWS_S3: 'aws_s3',
      AWS_GLUE: 'aws_glue',
      AWS_ATHENA: 'aws_athena',

    },
    LECTURE_7: {
      ID: 'lecture7',
      EMR_INTRO: 'introduction_to_aws_emr',
      EMR_ARCHITECTURE: 'EMR Architecture',
      EMR_OTHER_FEATURES: 'Other Features',
    },
    LECTURE_8: {
      ID: 'lecture8',
      ML_INTRO: 'introduction_to_spark_ml',
      ML_PIPELINES: 'designing-ml-pipelines',
      ML_EVALUATION: 'ml-evaluation',
      ML_EXAMPLES: 'ml-examples'
    },
    LECTURE_9: {
      ID: 'lecture9',
      DOCUMENTATION: 'Documentation',
      CD_CI: 'cd-ci',
      DATA_VALIDATION: "data-validation"
    },
    SUPPORT: {
      ID: 'support',
      GLOSSARY: 'glossary',
      RESOURCES: 'resources',
    }
  },
}


